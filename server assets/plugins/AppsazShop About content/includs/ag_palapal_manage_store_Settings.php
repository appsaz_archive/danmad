<?php


class ag_palapal_manage_store_Settings
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'add_menu_to_admin']);
        add_action('rest_api_init', function () {

            register_rest_route("ag_palapal", "get/aboutus", array('methods' => WP_REST_Server::ALLMETHODS, 'callback' => function () {
                header('Content-type: application/json; charset=utf-8');
                $aboutus = get_option("ag_aboutus_page");
                $aboutus = $aboutus ? $aboutus : '';
                echo json_encode(array("status" => 1, "response" => $aboutus));
                exit();
            }));
        });


    }


    public function add_menu_to_admin()
    {
        add_menu_page(' متن درباره ما', 'درباره ما اپلیکیشن', 'manage_options', 'ag_manage_store_app', function () {


            if (!current_user_can('manage_options')) {
                wp_die(__('you dont access to this page'));
            }


            if (isset($_REQUEST['aboutus_text'])) {
                $aboutus_text = $_REQUEST['aboutus_text'];

                update_option("ag_aboutus_page", $aboutus_text);
                echo '<div id="message" class="updated notice is-dismissible">
                        <p>با موفقیت ذخیره شد.</p>
                      </div>';
            }
            ?>
            <div class="wrap">
                <form name="ag__form1" id="ag__form1" method="post" action="">
                    <span>متن درباره ما :</span>
                    <textarea name="aboutus_text" id="aboutus_text" style="width: 100%;min-height: 400px;"
                    ><?php echo get_option("ag_aboutus_page"); ?></textarea>

                    <br>
                    <input type="submit" name="Submit" class="button-primary" value="ذخیره تغییرات"/>
                </form>
            </div>
            <?php

        }, 'dashicons-cart', 1);


    }


}
