<?php

add_action('admin_print_scripts', 'do_jslibs_notification');
add_action('admin_print_styles', 'do_css_notification');
function do_css_notification()
{
	wp_enqueue_style('thickbox');
}

function do_jslibs_notification()
{
	wp_enqueue_script('json2');
	wp_enqueue_script('jquery');
	wp_enqueue_script('editor');
	wp_enqueue_script('thickbox');
	add_action('admin_head', 'wp_tiny_mce');
}

