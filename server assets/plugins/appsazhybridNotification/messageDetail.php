<?php
require_once ('jdf.php');
if(!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

$detail_msgId=0;
if (isset ($_GET['detail_msgId'])){
	$detail_msgId=$_GET['detail_msgId'];
}

class Devices extends WP_List_Table
{

    public function __construct()
    {
        parent::__construct([
            'singular' => __('Device', 'sp'), //singular name of the listed records
            'plural' => __('Devices', 'sp'), //plural name of the listed records
            'ajax' => false //should this table support ajax?
        ]);
    }

    public static function get_devices($per_page, $page_number = 1)
    {
        global $wpdb;
        global $detail_msgId;
        $sql = "SELECT * FROM appsazhybrid_user_message usermsg INNER JOIN appsazhybrid_devices device ON usermsg.userId=device.userId WHERE usermsg.messageId=%d";
        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
        $results = $wpdb->get_results($wpdb->prepare($sql, $detail_msgId),'ARRAY_A');
        for($i = 0; $i < count($results); $i++) {
            $results[$i]['id']=$i+1;
            $sql = "SELECT delivery FROM appsazhybrid_user_message WHERE messageId=$detail_msgId AND userId='" . $results[$i]['userId']."'";
            $results[$i]['receivedMessages'] = $wpdb->get_var($sql);
            $sql = "SELECT seen FROM appsazhybrid_user_message WHERE messageId=$detail_msgId AND userId='" . $results[$i]['userId']."'";
            $results[$i]['seenMessages'] = $wpdb->get_var($sql);
        }
        return $results;
    }


    public static function record_count()
    {
        global $wpdb;
        $sql = "SELECT COUNT(*) FROM appsazhybrid_devices";
        return $wpdb->get_var($sql);
    }

    function get_columns()
    {
        $columns = array(
            'id' => 'شماره ردیف',
            'category' => 'دسته بندی',
            'deviceType' => 'نوع دستگاه',
            'receivedMessages' => 'پیام دریافت شده؟',
            'seenMessages' => 'پیام خوانده شده؟',
        );
        return $columns;
    }

    function column_default($item, $column_name)
    {
        switch($column_name) {
            case 'id':
                
            case 'category':
            case 'deviceType':
            case 'receivedMessages':
            case 'seenMessages':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
        }
    }

    function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $devicePerPage = 30;
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = self::get_devices($devicePerPage, $this->get_pagenum());
        $this->set_pagination_args(array(
            'total_items' => self::record_count(),                  //WE have to calculate the total number of items
            'per_page' => $devicePerPage
        ));
    }
}
function appsazMessageDetailPage()
{
    global $wpdb;
    global $detail_msgId;
    $query = "SELECT * FROM appsazhybrid_messages WHERE messageId=%d";
    $result=$wpdb->get_row($wpdb->prepare($query, [$detail_msgId]));
    $sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE messageId=" . intval($_GET['detail_msgId']);
    $sendCount = $wpdb->get_var($sql);
    ?>
    <div class="wrap">
        <style>
            table, th, td {
                border: 1px solid #ced1dd;
                border-collapse: collapse;
                padding: 10px;
            }
        </style>
		
		<div style="float:left">
			<a class="button button-primary" style="margin:10px 0px 10px 0px" href="<?php echo admin_url('admin.php?page=appsazMessageReports'); ?>">بازگشت</a>
		</div>
		
		
        <h1>جزئیات پیام</h1>
        <div style="padding: 10px">
            ارسال شده برای
            <?php echo $sendCount ?>
            نفر.
        </div>
        <table style="width:100%;">
            <tr>
                <td> <b>تاریخ ارسال</b></td>
                <td> <?php echo jdate('Y/m/d',strtotime($result->createdAt)) ?></td>
            </tr>
            <tr>
                <td> <b>عنوان</b></td>
                <td><?php echo $result->title ?></td>
            </tr>
            <tr>
                <td colspan="2"><b>محتوا</b></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo $result->content ?></td>
            </tr>
        </table>

        <div>
            <h1>لیست اعضای</h1>
            <?php
            $deviceList = new Devices();
            $deviceList->prepare_items();
            $deviceList->display();
            ?>
        </div>
    </div>
    <?php
}
