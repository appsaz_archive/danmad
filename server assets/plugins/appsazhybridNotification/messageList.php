<?php
//require_once(ABSPATH . 'jdf.php');
if(!class_exists('WP_List_Table')) {
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Message_List extends WP_List_Table
{
	public function __construct()
	{
		parent::__construct([
			'singular' => __('Message', 'sp'), //singular name of the listed records
			'plural' => __('Messages', 'sp'), //plural name of the listed records
			'ajax' => false //should this table support ajax?
		]);
	}

	public static function get_messages($per_page, $page_number = 1)
	{
		global $wpdb;
		$sql = "SELECT * FROM appsazhybrid_messages";

		if(!empty($_REQUEST['orderby'])) {
			$sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
			$sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
		} else
			$sql .= ' ORDER BY messageId DESC';
		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
		$results = $wpdb->get_results($sql, 'ARRAY_A');
		for($i = 0; $i < count($results); $i++) {
			$sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE messageId=" . $results[$i]['messageId'];
			$results[$i]['totalSend'] = $wpdb->get_var($sql);
			$sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE delivery=1 AND messageId=" . $results[$i]['messageId'];
			$results[$i]['totalDelivered'] = $wpdb->get_var($sql);
			$sql = "SELECT COUNT(*) FROM appsazhybrid_user_message WHERE seen=1 AND messageId=" . $results[$i]['messageId'];
			$results[$i]['totalSeen'] = $wpdb->get_var($sql);
		}
		return $results;
	}

	public static function record_count()
	{
		global $wpdb;
		$sql = "SELECT COUNT(*) FROM appsazhybrid_messages";
		return $wpdb->get_var($sql);
	}

	function get_columns()
	{
		$columns = array(
			'title' => 'عنوان',
			'createdAt' => 'تاریخ ارسال',
			'totalSend' => 'تعداد پیام ارسال شده',
			'totalDelivered' => 'تعداد پیام دریافت شده',
			'totalSeen' => 'تعداد پیام خوانده شده',
		);
		return $columns;
	}

	function column_default($item, $column_name)
	{
		switch($column_name) {
			case 'createdAt':
				return jdate('y/m/d ', strtotime(substr($item[$column_name],0, 10))) . ' ساعت ' . substr($item[$column_name], 11, 5);
			case 'title':
				return '<a href="' . admin_url('admin.php?page=appsazMessageDetail&detail_msgId=' . $item['messageId']) . '" style="display: block">' . $item[$column_name] . '</a>';
			case 'totalSend':
			case 'totalDelivered':
			case 'totalSeen':
				return $item[$column_name];
			default:
				return print_r($item, true); //Show the whole array for troubleshooting purposes
		}
	}

	function get_sortable_columns(){
		$columns=array(
			'title' => array('title', false),
			'createdAt' => array('createdAt', false)
		);
		return $columns;
	}

	function prepare_items()
	{
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$msgPerPage = 30;
		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = self::get_messages($msgPerPage, $this->get_pagenum());
		$this->set_pagination_args(array(
			'total_items' => self::record_count(),                  //WE have to calculate the total number of items
			'per_page' => $msgPerPage
		));
	}
}

function appsazMessageReportsPage()
{
	?>
	<div class="wrap">
		<h2>لیست پیام ها</h2>
		<?php
		$messageList = new Message_List();
		$messageList->prepare_items();
		$messageList->display();
		?>
	</div>
	<?php
}
