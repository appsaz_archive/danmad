"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var AppsazSettings = (function () {
    function AppsazSettings() {
        this.appNameFa = "Danmad Shop";
        this.appNameEng = "DanmadShop";
        this.advanced = true;
        this.demo = true;
        this.packageName = "ir.appsaz.shopproduct";
        this.bazaarPublish = false;
        this.googlePlayLink = "https://play.google.com/store/apps/details?id=" + this.packageName;
        this.bazaarLink = "http://cafebazaar.ir/app/?id=" + this.packageName;
        this.shareProductDownloadLink = this.googlePlayLink;
        this.shareLinkText = " Download ' "
            +
                this.appNameFa
            +
                " ' from below: "
            + " \n "
            + this.googlePlayLink;
        this.serverURL = "https://appsazshop.ir";
        this.ServerConsumerKey = "ck_3daf774d7f10499d6ade4f7fc1a08748efcede0a";
        this.ServerConsumerSecret = "cs_a3997c84e5cfe7587e1bc2ab08e74ee4071e1b0d";
        this.appVersion = "0.1.2";
        this.srcVersion = "3.6.0"; //pls  dont change
        this.gifsplash = false;
        this.gifsplashDuration = 8000;
        this.imgsplash = true;
        this.enableNotif = true;
        this.aboutText = "";
        this.nopriceTag = "Comming soon...";
        this.callonNoprice = false;
        this.imgTitle = false;
        this.addtocartAsGuest = true;
        this.contactUsUrl = "https://appsazshop.ir/contact-us/";
        this.policyUrl = "https://appsazshop.ir/%D8%B4%D8%B1%D8%A7%DB%8C%D8%B7-%D9%88-%D9%82%D9%88%D8%A7%D9%86%DB%8C%D9%86/";
        if (this.bazaarPublish) {
            this.googlePlayLink = this.bazaarLink;
        }
        this.shareLinkText = " Download ' "
            +
                this.appNameFa
            +
                " ' from below: "
            + " \n "
            + this.googlePlayLink;
        this.shareProductDownloadLink = this.googlePlayLink;
    }
    AppsazSettings = __decorate([
        core_1.Injectable()
    ], AppsazSettings);
    return AppsazSettings;
}());
exports.AppsazSettings = AppsazSettings;
//# sourceMappingURL=appsazSettings.js.map