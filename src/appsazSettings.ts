import { Injectable } from '@angular/core';

@Injectable()
export class AppsazSettings {

   public appNameFa : any ="Danmad Shop";
   public appNameEng : any ="Danmad Shop";
   public advanced : boolean = true;
   public demo : boolean = false;
   public packageName : any ="com.appsaz.danmad";
   public bazaarPublish : boolean =  false;
   public googlePlayLink : any ="https://play.google.com/store/apps/details?id="+this.packageName;
   public bazaarLink : any ="http://cafebazaar.ir/app/?id="+this.packageName;
   public shareProductDownloadLink : any = this.googlePlayLink;
   public shareLinkText : any =" Download ' "
       +
       this.appNameFa
      +
       " ' from below: "
       +" \n "
       +this.googlePlayLink;
   public serverURL : any ="https://danmad.xyz";
   public ServerConsumerKey : any ="ck_3daf774d7f10499d6ade4f7fc1a08748efcede0a";
   public ServerConsumerSecret : any ="cs_a3997c84e5cfe7587e1bc2ab08e74ee4071e1b0d";
   public appVersion : any ="1.1.5";
   public srcVersion : any ="3.6.0"; //pls  dont change
   public gifsplash : boolean = true;
   public gifsplashDuration : any = 4500;
   public imgsplash : boolean = false;
   public enableNotif : boolean = true;
   public aboutText : string = "";
   public nopriceTag : string = "Comming soon";
   public callonNoprice : boolean =  false;
   public imgTitle : boolean =  false;
   public addtocartAsGuest : boolean =  true;

   public contactUsUrl : any =  "https://danmad.xyz/contact-us/";
   public policyUrl : any =  "https://danmad.xyz/%D8%B4%D8%B1%D8%A7%DB%8C%D8%B7-%D9%88-%D9%82%D9%88%D8%A7%D9%86%DB%8C%D9%86/";


    constructor() {

       if(this.bazaarPublish){
          this.googlePlayLink = this.bazaarLink;
       }

    this.shareLinkText  =" Download ' "
           +
           this.appNameFa
           +
           " ' from below: "
           +" \n "
           +this.googlePlayLink;


       this.shareProductDownloadLink = this.googlePlayLink;
    }



}
