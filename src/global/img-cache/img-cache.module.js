"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var lazy_img_component_1 = require('./lazy-img.component');
var lazy_img_directive_1 = require('./lazy-img.directive');
var img_cache_service_1 = require('./img-cache.service');
var declarations = [
    lazy_img_component_1.LazyImgComponent,
    lazy_img_directive_1.LazyLoadDirective
];
var CacheImgModule = (function () {
    function CacheImgModule(parentModule) {
    }
    CacheImgModule.forRoot = function () {
        return {
            ngModule: CacheImgModule,
            providers: [
                img_cache_service_1.ImgCacheService
            ]
        };
    };
    CacheImgModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: declarations.slice(),
            exports: declarations.slice()
        }),
        __param(0, core_1.Optional()),
        __param(0, core_1.SkipSelf())
    ], CacheImgModule);
    return CacheImgModule;
}());
exports.CacheImgModule = CacheImgModule;
//# sourceMappingURL=img-cache.module.js.map