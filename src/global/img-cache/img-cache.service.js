"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var imgcache_js_1 = require('@chrisben/imgcache.js');
var ReplaySubject_1 = require('rxjs/ReplaySubject');
var operators_1 = require('rxjs/operators');
var bindCallback_1 = require('rxjs/observable/bindCallback');
var utils_1 = require('../utils');
/**
 * This service is charged of provide the methods to cache the images
 */
var ImgCacheService = (function () {
    function ImgCacheService(platform, file) {
        this.platform = platform;
        this.file = file;
        this.initNotifier$ = new ReplaySubject_1.ReplaySubject();
        // change on production mode
        imgcache_js_1["default"].options.debug = true;
    }
    Object.defineProperty(ImgCacheService.prototype, "notifier$", {
        get: function () {
            return this.initNotifier$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Init imgCache library
     * @return {Promise}
     */
    ImgCacheService.prototype.initImgCache = function () {
        var _this = this;
        var init$ = bindCallback_1.bindCallback(imgcache_js_1["default"].init)();
        return init$.pipe(operators_1.take(1), operators_1.tap(function () { return _this.initNotifier$.next('init'); }));
    };
    /**
     * Cache is image by using a url. If the image is already cached this method
     * will return the URL
     * @param src {string}
     */
    ImgCacheService.prototype.cache = function (src) {
        var _this = this;
        return this.notifier$.pipe(operators_1.switchMapTo(this.isCached(src)
            .pipe(operators_1.flatMap(function (_a) {
            var path = _a[0], success = _a[1];
            return success ? _this.getCachedFileURL(path) : _this.cacheFile(path);
        }), operators_1.map(function (url) {
            if (_this.platform.is('ios')) {
                return _this.normalizeURlWKWview(url);
            }
            return url;
        }))));
    };
    /**
     * WKWebview doesn't accept urls with file;// or cvdfile:// protocols.
     * The url is formatted and fix to find the img
     * @param url
     * @return {string} - url formatted
     */
    ImgCacheService.prototype.normalizeURlWKWview = function (url) {
        var urlIos = utils_1.normalizeUrlIos(this.file.applicationStorageDirectory) + "Library/files/" + utils_1.normalizeUrlIos(url);
        return urlIos.replace('/localhost/persistent', '');
    };
    /**
     * Get file URL if cached
     * @param src - image url
     */
    ImgCacheService.prototype.getCachedFileURL = function (src) {
        return bindCallback_1.bindCallback(imgcache_js_1["default"].getCachedFileURL)(src)
            .pipe(operators_1.map(function (urls) { return urls[1]; }));
    };
    /**
     * Cache image using a url
     * @param src {string}
     */
    ImgCacheService.prototype.cacheFile = function (src) {
        return bindCallback_1.bindCallback(imgcache_js_1["default"].cacheFile)(src);
    };
    /**
     * Check if image is already cached
     * @param src
     */
    ImgCacheService.prototype.isCached = function (src) {
        return bindCallback_1.bindCallback(imgcache_js_1["default"].isCached)(src);
    };
    ImgCacheService = __decorate([
        core_1.Injectable()
    ], ImgCacheService);
    return ImgCacheService;
}());
exports.ImgCacheService = ImgCacheService;
//# sourceMappingURL=img-cache.service.js.map