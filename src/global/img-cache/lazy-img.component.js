"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
/**
 * Component in charge of lazy load images and cache them
 */
var LazyImgComponent = (function () {
    function LazyImgComponent() {
        this.placeholderActive = true;
    }
    __decorate([
        core_1.Input()
    ], LazyImgComponent.prototype, "source");
    LazyImgComponent = __decorate([
        core_1.Component({
            selector: 'op-lazy-img',
            template: "\n  <div text-center [ngClass]=\"{ 'placeholder': placeholderActive }\">\n    <img op-lazy-img [source]=\"source\" (loaded)=\"placeholderActive = false\"/>\n  </div>\n  "
        })
    ], LazyImgComponent);
    return LazyImgComponent;
}());
exports.LazyImgComponent = LazyImgComponent;
//# sourceMappingURL=lazy-img.component.js.map