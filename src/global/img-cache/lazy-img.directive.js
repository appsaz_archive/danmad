"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
/**
* This directive is charge of cache the images and emit a loaded event
*/
var LazyLoadDirective = (function () {
    function LazyLoadDirective(el, imgCacheService, renderer) {
        this.el = el;
        this.imgCacheService = imgCacheService;
        this.renderer = renderer;
        this.source = '';
        this.loaded = new core_1.EventEmitter();
    }
    LazyLoadDirective.prototype.ngOnInit = function () {
        var _this = this;
        // get img element
        var nativeElement = this.el.nativeElement;
        // add load listener
        this.loadListener = this.renderer.listen(nativeElement, 'load', function () {
            _this.renderer.addClass(nativeElement, 'loaded');
            _this.loaded.emit();
        });
        this.errorListener = this.renderer.listen(nativeElement, 'error', function () {
            nativeElement.remove();
        });
        // cache img and set the src to the img
        this.cacheSubscription =
            this.imgCacheService
                .cache(this.source)
                .subscribe(function (value) {
                _this.renderer.setAttribute(nativeElement, 'src', value);
            }, function (e) { return console.log(e); });
    };
    LazyLoadDirective.prototype.ngOnDestroy = function () {
        // remove listeners
        this.loadListener();
        this.errorListener();
        this.cacheSubscription.unsubscribe();
    };
    __decorate([
        core_1.Input('source')
    ], LazyLoadDirective.prototype, "source");
    __decorate([
        core_1.Output()
    ], LazyLoadDirective.prototype, "loaded");
    LazyLoadDirective = __decorate([
        core_1.Directive({
            selector: '[op-lazy-img]'
        })
    ], LazyLoadDirective);
    return LazyLoadDirective;
}());
exports.LazyLoadDirective = LazyLoadDirective;
//# sourceMappingURL=lazy-img.directive.js.map