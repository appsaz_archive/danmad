"use strict";
function normalizeUrlIos(url) {
    return (url).replace(/(cdvfile|file):\/\//g, '');
}
exports.normalizeUrlIos = normalizeUrlIos;
//# sourceMappingURL=url.util.js.map