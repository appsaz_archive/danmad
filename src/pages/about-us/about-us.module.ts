import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutUsPage } from './about-us';
import {CacheImgModule} from "../../global/img-cache";

@NgModule({
  declarations: [

  ],
  imports: [
    CacheImgModule,
    IonicPageModule.forChild(AboutUsPage),
  ],
})
export class AboutUsPageModule {}
