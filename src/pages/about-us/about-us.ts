import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SocialSharing} from "@ionic-native/social-sharing";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {CartPage} from "../cart/cart";
import {Values} from "../../providers/service/values";
import {AppsazSettings} from "../../appsazSettings";
import {Service} from "../../providers/service/service";
import {LocalCartPage} from "../local-cart/local-cart";


@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
  })

export class AboutUsPage {

  public aboutText : any =  "...";
  public loading : any =  true;



  constructor( public navCtrl: NavController,
               public service : Service,
               public navParams: NavParams,
               private socialSharing: SocialSharing,
               private iab: InAppBrowser,
               public nav: NavController,
               public values: Values ,
               public appsazSetings : AppsazSettings) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUsPage');

    if(this.appsazSetings.advanced){
      this.service.getAboutText().then(data=>{
        this.aboutText =  data.response;
        this.loading=false;
      });
    }
    else{
      this.aboutText =  this.appsazSetings.aboutText;
    }

  }

  loadSite() {
    console.log("load site called")
    var target = "_system";

    try {
      const browser = this.iab.create("http://appsaz.ir", target, 'location=no,hardwareback=no,EnableViewPortScale=yes,closebuttoncaption=close');
    }
    catch (error) {
      console.log("inApp browser error");
    }
  }

  getCart() {
    this.nav.push(LocalCartPage);
  }

}
