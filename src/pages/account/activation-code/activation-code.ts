import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import {Service} from "../../../providers/service/service";
import {Values} from "../../../providers/service/values";
import {Functions} from "../../../providers/service/functions";
import {AccountRegister} from "../register/register";
import {CartPage} from "../../cart/cart";
import {Home} from "../../home/home";
import {AccountLogin} from "../login/login";
import {AppsazSettings} from "../../../appsazSettings";
import {AccountsService} from "../../../providers/service/accounts-service";
import { GooglePlus } from '@ionic-native/google-plus';
import {LocalCartPage} from "../../local-cart/local-cart";

/**
 * Generated class for the ActivationCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-activation-code',
    templateUrl: 'activation-code.html',
})
export class ActivationCodePage {
    step: number = 1;
    phoneNumber: any;
    activationCode: number;
    disableButton: boolean = false;
    disableButton2: boolean = false;
    action: string;
    private newPass: any;
    private rePass: any;
    private resendable: boolean = false;
    private countDown: number = 60;
    registerData: any;
    loginStatus: any;
    errors: any;
    disableBtn: boolean = false;

    constructor(public appsazSettings: AppsazSettings,
                public navCtrl: NavController,
                public functions: Functions,
                public navParams: NavParams,
                public service: Service,
                public accountsService : AccountsService,
                public values: Values,
                private googlePlus: GooglePlus,
                public alertCtrl: AlertController) {

        this.action = this.navParams.get("action");

        this.registerData = {};
        this.registerData.billing_address = {};
        this.registerData.shipping_address = {};
        this.registerData.billing_address.phone = "";
        this.registerData.billing_address.country = "";

    }


    sendActivationCode() {
        var num: any;
        num = this.phoneNumber.toString();

        /*if (!num.startsWith("9") || (num.length != 10) || !this.phoneNumber) {*/
        /* if (false) {
         /!*  this.functions.showToast("شماره تلفن وارد شده معتبر نمیباشد.")*!/
         this.functions.showToast("Phone number is not valid.")
         } else {*/

        //num = "0" + num;
        this.phoneNumber = num;
        this.disableButton = true;


        if (this.action == "forgetPass") {
            this.accountsService.generateForgetCode(this.phoneNumber).then(
                data => {

                    if (data.response == "1") {
                        /* this.functions.showToast("کد اختصاصی تا لحظاتی دیگر به شماره شما ارسال خواهد شد.");*/
                        this.functions.showToast("Verification code will send to you");
                        this.step = 2;
                        this.resendable = false;
                        this.StartTimer();
                    }
                    if (data.response == "-1") {
                        /* this.functions.showToast("شما با این شماره تلفن در سیستم ثبت نام نکرده اید.");*/
                        this.functions.showToast("You are not registered before.");
                        this.disableButton = false;
                    }

                }, error => {
                    console.log(error);
                    this.disableButton = false;
                    /*this.functions.showToast("خطا در برقراری ارتباط");*/
                    this.functions.showToast("Connection error");
                });
        }


        if (this.action == "register") {
            this.accountsService.generateActivationCode(this.phoneNumber).then(
                data => {

                    if (data.response == "1") {
                        /* this.functions.showToast("کد فعالسازی تا لحظاتی دیگر به شماره شما ارسال خواهد شد.");*/
                        this.functions.showToast("Verification code will send to you");
                        this.step = 2;
                        this.StartTimer();
                    }
                    if (data.response == "-1") {
                        /* this.functions.showToast("شما قبلا با این شماره تلفن ثبت نام کرده اید.");*/
                        this.functions.showToast("You have been registered before.");
                        this.functions.showToast("");
                        this.disableButton = false;
                    }


                }, error => {
                    this.disableButton = false;
                    /*this.functions.showToast("خطا در برقراری ارتباط");*/
                    this.functions.showToast("Connection error");
                });



        }

        // }

    }

    validateActivationCode() {
        this.disableButton2 = true;

        if (this.action == "forgetPass") {

            if (this.newPass == this.rePass) {

                this.accountsService.checkForgetCode(this.activationCode, this.phoneNumber, this.newPass).then(
                    data => {

                        if (data['response'] == "1") {
                            /* this.functions.showToast("رمز عبور با موفقیت تغییر یافت.");*/
                            this.functions.showToast("Password updated successfully");
                            this.navCtrl.setRoot(Home);
                        } else if (data['response'] == 0) {
                            /*this.functions.showToast("کد وارد شده اشتباه است.");*/
                            this.functions.showToast("Code is not correct.");
                            this.disableButton2 = false;
                        }

                    },
                    error => {
                        console.log(error);
                        this.disableButton2 = false;
                        /*this.functions.showToast("خطا در برقراری ارتباط");*/
                        this.functions.showToast("Connection error");
                    })
            } else {
                /*this.functions.showToast("تکرار رمز تطابق ندارد.");*/
                this.functions.showToast("Password not match.");
                this.disableButton2 = false;
            }


        }

        if (this.action == "register") {

            this.accountsService.checkActivationCode(this.activationCode, this.phoneNumber).then(
                data => {
                    if (data['activation'] == 1) {
                        this.navCtrl.push(AccountRegister, {"phoneNumber": this.phoneNumber});
                    } else if (data['activation'] == 0) {
                        /*this.functions.showToast("کد فعال سازی اشتباه است.");*/
                        this.functions.showToast("Activation code is not correct");
                        this.disableButton2 = false;
                    }

                },
                error => {
                    this.disableButton2 = false;
                    /* this.functions.showToast("خطا در برقراری ارتباط");*/
                    this.functions.showToast("Connection error");
                    console.log(error);
                })


        }


    }

    editNumber() {
        this.phoneNumber='';
        this.step = 1;
        this.disableButton = false;
    }

    resendCode() {
        this.resendable = false;
        this.sendActivationCode();
        this.StartTimer();
        this.disableButton2 = false;

    }

    getCart() {
        this.navCtrl.push(LocalCartPage);
    }


    StartTimer() {

        var id = setInterval(() => {
            if (this.countDown > 0) {
                this.countDown -= 1;
            } else {

                this.countDown = 60;
                this.resendable = true;
                clearInterval(id);
            }
        }, 1000);
    }




    googlePlusLogin(){
        this.disableBtn= true;
        this.googlePlus.disconnect().then(
            (msg) => {
                //alert("disconnect: "+ JSON.stringify(msg));
                this.loginGoogleFunction();
            }).catch(
            (msg) => {
                //alert("logout error: "+ JSON.stringify(msg));
                this.loginGoogleFunction();
            })


        //console.log("errorr ", err)

    }



    loginGoogleFunction(){
        this.googlePlus.login({})
            .then(res =>{
                    //alert("res"+ JSON.stringify(res))
                    this.registerData.username= res.email;
                    this.registerData.password= 'googlePlusLogin';
                    this.registerData.googlePlusLogin= true;
                    this.registerData.email= res.email;
                    this.registerData.billing_address.email = res.email;
                    this.registerData.billing_address.phone= res.email;
                    this.registerData.billing_address.first_name = res.givenName;
                    this.registerData.billing_address.last_name = res.familyName;
                    this.registerData.shipping_address.first_name = res.givenName;
                    this.registerData.shipping_address.last_name = res.familyName;
                    this.registerData.shipping_address.company = "";
                    this.registerData.shipping_address.address_1 = "";
                    this.registerData.shipping_address.address_2 = "";
                    this.registerData.shipping_address.city = "";
                    this.registerData.shipping_address.state = "";
                    this.registerData.shipping_address.postcode = "";
                    this.registerData.shipping_address.country = "";
                    //this.disableSubmit = true;
                    this.accountsService.registerCustomer(this.registerData)
                        .then((results) => {
                            this.handleRegister(results);
                            //alert("res2"+ JSON.stringify(results))
                        })
                    //console.log("res", res);
                }
            )
            .catch(err =>{
                this.disableBtn= false;
                //console.error(err)
                //alert("errorr "+ err)
                /*this.registerData.googlePlusLogin= true;
                 this.registerData.username= 'email2@gmail.com';
                 this.registerData.email= 'email2@gmail.com';
                 this.registerData.password= 'googlePlusLogin';
                 this.registerData.billing_address.email = 'email222@gmail.com';
                 this.registerData.billing_address.phone = 'email222@gmail.com';
                 this.registerData.billing_address.first_name = 'givenName';
                 this.registerData.billing_address.last_name = 'familyName';
                 this.registerData.shipping_address.first_name = 'givenName';
                 this.registerData.shipping_address.last_name = 'familyName';
                 this.registerData.shipping_address.company = "";
                 this.registerData.shipping_address.address_1 = "";
                 this.registerData.shipping_address.address_2 = "";
                 this.registerData.shipping_address.city = "";
                 this.registerData.shipping_address.state = "";
                 this.registerData.shipping_address.postcode = "";
                 this.registerData.shipping_address.country = "";
                 //this.checkUserRegisterdBefore();
                 //this.disableSubmit = true;
                 this.accountsService.registerCustomer(this.registerData)
                 .then((results) => {
                 this.handleRegister(results);
                 alert("res"+ JSON.stringify(results))
                 })*/
            });
    }


    handleRegister(results) {
        console.log(results.errors);
        //this.disableSubmit = false;
        this.disableBtn= false;
        if (!results.errors) {
            this.accountsService.login(this.registerData)
                .then((results) => this.loginStatus = results);
            this.navCtrl.setRoot(Home);
        }
        else if (results.errors) {
            this.errors = results.errors;
            if(results.errors[0]['message']){
                //this.functions.showAlertError('',results.errors[0]['message'])
                let alert = this.alertCtrl.create({
                    subTitle: 'An account is already registered with that username. Please choose another or login',
                    buttons: [
                        {
                            text: 'Change email',
                            handler: () => {
                                return;
                            }
                        },
                        {
                            text: 'Login',
                            handler: () => {
                                this.navCtrl.push(AccountLogin);
                            }
                        }
                    ]
                });
                alert.present();
            }
        }
    }


}
