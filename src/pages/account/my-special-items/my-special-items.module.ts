import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySpecialItemsPage } from './my-special-items';

@NgModule({
  declarations: [
    //MySpecialItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(MySpecialItemsPage),
  ],
})
export class MySpecialItemsPageModule {}
