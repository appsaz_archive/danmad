import {Component} from '@angular/core';
import {
    IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController,
    ToastController
} from 'ionic-angular';
import {AppsazSettings} from "../../../appsazSettings";
import {CategoryService} from "../../../providers/service/category-service";
import {Service} from "../../../providers/service/service";
import {Values} from "../../../providers/service/values";
import {Functions} from "../../../providers/service/functions";
import {Network} from "@ionic-native/network";
import {OfflinePage} from "../../offline/offline";
import {ProductService} from "../../../providers/service/product-service";
import {ProductPage} from "../../product/product";
import {CartPage} from "../../cart/cart";
import {CartService} from "../../../providers/service/cart-service";
import {AccountsService} from "../../../providers/service/accounts-service";
import {toArray} from "rxjs/operators";

import {Plugins} from "@capacitor/core";
import {LocalCartServiceProvider} from "../../../providers/service/local-cart-service";
import {LocalCartPage} from "../../local-cart/local-cart";

const {Storage} = Plugins;


@IonicPage()
@Component({
    selector: 'page-my-special-items',
    templateUrl: 'my-special-items.html',
})
export class MySpecialItemsPage {
    products: any = [];
    count: any;
    offset: any;
    category: any;
    has_more_items: boolean = true;
    status: any;
    options: any;
    categories: any;
    subCategories: any;
    items: any;
    quantity: any;
    q: any;
    showFilters: boolean = false;
    data: any;
    sort: number = 0;
    categoryName: any;
    title: boolean;
    filterModal: any;
    sortValue: number = 2;
    sortValueArray: any = [false, false, true, false];
    firstPage: boolean = true;
    pageParams: any;
    filter_types: any = false;
    selectedBrand: any;
    selectedCategory: any;
    loading: boolean = true;
    cart: any;
    disableBtn: boolean = false;


    constructor(public appsazSettings: AppsazSettings,
                public nav: NavController,
                public popoverCtrl: PopoverController,
                public service: Service,
                public publicService: Service,
                private network: Network,
                params: NavParams,
                public values: Values,
                public functions: Functions,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private productService: ProductService,
                public cartService: CartService,
                public localCartService: LocalCartServiceProvider,
                public accountsService: AccountsService) {

        // this.data = {};
        this.q = "";
        this.pageParams = params;
        this.count = 10;
        this.offset = 0;
        this.values.filter = {};
        this.options = [];
        this.subCategories = [];
        this.items = [];
        this.quantity = 1;
        this.products = [];
        this.has_more_items = true;


        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }

        this.publicService.filter_types = [];


    }

    ionViewDidLoad() {
        this.getData(this.pageParams);
    }


    getData(params) {
        this.offset += 1;
        this.firstPage = true;
        this.service.getSpecialItems(this.offset, this.count).then(data => {
            console.log("data", data);
            console.log("typeof data", typeof data);
            for (let key in data) {
                console.log(key); // logs keys in myObject
                console.log(data[key]); // logs values in myObject

                let qnty =this.localCartService.getProductQuantityInCart(data[key].id);
                if(qnty){
                    data[key].quantity = qnty;
                }else{
                    data[key].quantity = 0;
                }

                this.products.push(data[key]);
            }

            // this.products.map(product => {
            //     product.quantity = 0;
            //     return product;
            // });

            /* for(var i=0; i<data.length; i++){
               this.products.push(data[i]);
             }*/
            //this.products.push(data);
            this.loading = false;
            // this.cartService.loadCart().then(results => {
            // this.handleCartInit();
            // },
            // failure => {
            // });
        });
    }


    doInfinite(infiniteScroll) {
        this.offset += 1;
        this.service.getSpecialItems(this.offset, this.count).then(data => {

            //var data2= Object.entries(data);
            console.log("AAA", typeof data, data)

            for (var key in data) {
                console.log(key); // logs keys in myObject
                console.log(data[key]); // logs values in myObject

                let qnty =this.localCartService.getProductQuantityInCart(data[key].id);
                if(qnty){
                    data[key].quantity = qnty;
                }else{
                    data[key].quantity = 0;
                }

                this.products.push(data[key]);
            }

            if (Object.keys(data).length > 0) {
                this.loading = false;
                // this.handleCartInit();
                infiniteScroll.complete();
            } else {
                this.has_more_items = false;
            }

            /*if(data.length>0){
             for(var i=0; i<data.length; i++){
               this.products.push(data[i]);
             }
             console.log("this.products2: ", this.products)
             this.loading= false;
             this.has_more_items= noItem;
             this.checkItemsInCart();
             infiniteScroll.complete();
              }
              else{
              this.has_more_items= false;
              }*/
        });
    }

    getProduct(id) {
        this.nav.push(ProductPage, id.id);
    }

    getCart() {
        this.disableBtn = false;
        this.nav.push(LocalCartPage);
    }


    handleCartInit() {
        // Storage.get({key: 'cart'}).then(data => {
        //     let data_in_cart = JSON.parse(data.value);
        this.products.map(product => {
            let product_quantity_in_cart = this.localCartService.getProductQuantityInCart(product.id);
            if (product_quantity_in_cart) {
                product.quantity = product_quantity_in_cart;
            }

            return product;
        });

        console.dir(this.products);
        // });
    }

    checkItemsInCart() {

        console.log("this.products.length", this.products.length)
        let i = (this.offset - 1) * this.count;
        console.log("i", i)
        console.log("this.cart", this.cart);
        for (i; i < this.products.length; i++) {
            this.products[i].quantity = 0;
            console.log("cart.cart_contents.length", typeof this.cart.cart_contents, this.cart.cart_contents)
            for (let j = 0; j < this.cart.cart_contents.length; j++) {
                console.log(i + " : " + this.products[i].id, ": ", this.cart.cart_contents[j][1].product_id)
                /*if(this.products[i].id == this.cart.cart_contents[j][1].product_id){
                  this.products[i].quantity = this.cart.cart_contents[j][1].quantity;
                }*/
            }
        }

    }


    changetodo(quantity, i) {
        console.log(this.products[i].quantity);
        if (this.products[i].stock_quantity != null && this.products[i].stock_quantity > 0 && quantity > this.products[i].stock_quantity) {
            this.functions.showAlertError('', 'The specified amount cannot be greater than amount of stock')
            this.products[i].quantity = this.products[i].stock_quantity;
        } else if (quantity < 0) {
            this.functions.showAlertError('', 'The specified amount cannot be smaller than zero')
            this.products[i].quantity = 0;
        }
    }


    plusCount(quantity, i) {
        this.products[i].quantity++
        setTimeout(() => {
            this.changetodo(quantity, i);
        }, 500)
    }

    minusCount(quantity, i) {
        this.products[i].quantity--
        setTimeout(() => {
            this.changetodo(quantity, i)
        }, 500)
    }


    async UpdateCartNew() {
        this.disableBtn = true;
        // this.localCartService.emptyCart();

        for (const product of this.products) {
            if (product.quantity > 0) {
                await this.localCartService.addLocal(product.id, product.quantity, false);
            }
        }

        await this.localCartService.calculate();
        this.getCart();

        // let options: any = {};
        // options.products_list = [];
        // /*   options.products_list = [
        //  {product_id: 4367, quantity: 2},
        //  {product_id: 4365, quantity: 2},
        //  ];
        //  */
        // for (var i = 0; i < this.products.length; i++) {
        //     if (this.products[i].quantity > 0) {
        //         options.products_list.push({product_id: this.products[i].id, quantity: this.products[i].quantity})
        //         // this.products[i].quantity = this.cart.cart_contents[j][1].quantity;
        //     }
        // }
        // console.log("options:", options);
        // //this.service.addToCartBulk(options).then((results) => this.updateCart(results));
        // this.accountsService.emptyCart()
        //     .then((results) => {
        //         this.values.count = 0;
        //         this.service.addToCartBulk(options).then((results) => this.handleAddProductsToCart(results));
        //     })
        //
        //
        // /*  this.service.addToCartBuilk()
        //  .then((results) => this.handleAddProductsToCart(results));
        //  /!*     this.accountsService.emptyCart()
        //  .then((results) => {
        //  this.values.count= 0;
        //  })*!/*/
    }


    handleAddProductsToCart(results) {
        if (!results.success || results.success == false) {
            this.functions.showAlertError("", results.msg);
        } else if (results.success && results.success == true) {
            //this.functions.showAlertSuccess("", 'Your orders submitted successfully');
            //this.values.count ++;
            this.getCart();
        }
    }


    addToCart(product, index) {
        // this.disableBtn= true;
        //this.selectedItemIndex= index;
        this.addToCartFunction(product, index);
    }


    addToCartFunction(product, index) {
        console.log("111 ", product)
        /* this.options = [];
         this.options.product_id = product.id;
         this.productService.addToCart(this.options).then((results) => {
         this.updateCart(results);
         });*/
        //this.product = product;
        this.options = [];
        this.options.product_id = product.id;
        if (this.values.isLoggedIn) {
            if (product.stock_amount != 0) {
                //if (this.setVariations()) {
                console.log(this.options)
                this.productService.addToCart(this.options).then((results) => {
                    this.updateCart(results);
                    this.products[index].quantity++;
                });
                //}
            }
        } else {
            //this.showLoginToast();
        }
    }

    updateCart(a) {
        if (!a.error) {
            this.showCartToast();
            this.values.count += parseInt('1');
        }
        /* this.disableSubmit = false;
         this.addToCartLoading = false;*/
    }


    showCartToast() {
        let toast = this.toastCtrl.create({
            message: 'Added to cart.',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            closeButtonText: "view cart",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(LocalCartPage);
            }
        });
        toast.present();
    }


}
