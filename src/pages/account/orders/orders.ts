import {Component} from '@angular/core';
import {AlertController, NavController, ModalController} from 'ionic-angular';
import {Service} from '../../../providers/service/service';
import {Values} from '../../../providers/service/values';
import {Functions} from '../../../providers/service/functions';
import {OrderDetails} from '../order-details/order-details';
import {CartPage} from "../../cart/cart";
import {ProductService} from '../../../providers/service/product-service';
import {AccountsService} from "../../../providers/service/accounts-service";
import {ReorderModalPage} from "../reorder-modal/reorder-modal";
import {LocalCartServiceProvider} from "../../../providers/service/local-cart-service";
import {LocalCartPage} from "../../local-cart/local-cart";

@Component({
    templateUrl: 'orders.html',
})
export class Orders {
    orders: any;
    count: any;
    offset: any;
    has_more_items: boolean = true;
    quantity: any;
    page: number = 1;
    id: any;
    filter: any;
    status: any;

    orderItems: any = {};
    orderChanges: boolean = false;
    orderItemsIDs: any = [];
    product: any;
    options: any;
    reorderModal: any;
    disableBtn: boolean = false;
    selectedOrderIndex: number;

    constructor(public nav: NavController,
                public alertCtrl: AlertController,
                public service: Service,
                public values: Values,
                public functions: Functions,
                public productService: ProductService,
                public modalCtrl: ModalController,
                public localCartService: LocalCartServiceProvider,
                public accountsService: AccountsService) {

        this.filter = {};
        this.filter.page = 1;
        this.count = 10;
        this.offset = 0;
        this.quantity = "1";

        this.filter.customer = this.service.loggedInCustomer.customer.id.toString();

        this.service.getOrders(this.filter)
            .then((results) => {
                this.orders = results;
                console.log(this.orders);
            });
    }

    doInfinite(infiniteScroll) {
        this.filter.page += 1;
        console.log("filter.page", this.filter.page)
        this.service.getOrders(this.filter)
            .then((results) => this.handleMore(results, infiniteScroll));
    }

    handleMore(results, infiniteScroll) {
        this.filter.page += 1;
        /* if (results.orders != undefined) {
             for (var i = 0; i < results.orders.length; i++) {
                 this.orders.orders.push(results.orders[i]);
             };
         }*/
        if (results != undefined) {
            for (var i = 0; i < results.length; i++) {
                this.orders.push(results[i]);
            }
            ;
        }
        if (results.length == 0) {
            this.has_more_items = false;
        }
        infiniteScroll.complete();
    }

    getOrderDetails(id) {
        this.nav.push(OrderDetails, id);
    }

    cancelOrder(id) {

        let alert = this.alertCtrl.create({
            subTitle: 'Are you sure you want to cancel this order',
            buttons: [
                {
                    text: ' Yes',
                    handler: () => {
                        this.service.updateOrder({
                            "order": {
                                "status": "cancelled"
                            }
                        }, id)
                            .then((results) => this.handleCancelOrder(results));
                    }
                },
                {
                    text: 'No',
                    handler: () => {

                    }
                }
            ]
        });
        alert.present();

    }

    handleCancelOrder(results) {
        this.functions.showAlertSuccess("", "Order canceled successfully.");
        this.service.getOrders(this.filter)
            .then((results) => this.orders = results);
    }

    reOrder(id) {
        this.service.updateOrder({
            "order": {
                "status": "pending"
            }
        }, id)
            .then((results) => this.handleReOrder(results));
    }

    handleReOrder(results) {
        this.functions.showAlertSuccess("", "Order submitted successfully.");
        this.service.getOrders(this.filter)
            .then((results) => this.orders = results);
    }


    getCart() {
        this.nav.push(LocalCartPage);
    }


    orderAgain(order_id, i) {
        this.disableBtn = true;
        this.selectedOrderIndex = i;
        this.service.checkOrderItemsToReorder(order_id)
            .then((results) => this.handleOrderAgain(results, order_id));
    }

    handleOrderAgain(results, order_id) {
        if (!results.success || results.success == false) {
            this.functions.showAlertError("", results.msg);
        } else if (results.success && results.success == true) {
            this.orderItems = results.items;
            if (results.items != undefined) {
                this.getProductsId(results);
            }
            if (results.is_order_items_changed == false) {
                //add
                console.log("orderItems: ", this.orderItemsIDs)
                this.getConfirmToReorder(order_id);
            } else if (results.is_order_items_changed == true) {
                //this.functions.showAlertWarning("", "Some items have been changed.");
                this.orderChanges = true;
                this.openModal();
                //"changed"
            }
        }
        this.disableBtn = false;

        /*this.functions.showAlertSuccess("", "سفارش با موفقیت ثبت شد.");
         this.service.getOrders(this.filter)
         .then((results) => this.orders = results);*/
    }


    openModal() {
        //TODO
        this.reorderModal = this.modalCtrl.create(ReorderModalPage, {orderItems: this.orderItems});
        this.reorderModal.present();
        this.reorderModal.onDidDismiss(data => {
        });
    }

    getProductsId(results) {
        this.orderItemsIDs = [];
        this.orderItemsIDs = results.items;
        /*for (var i = 0; i < results.items.length; i++) {
            this.orderItemsIDs.push(results.items[i].product_id);
        }*/
    }


    getConfirmToReorder(order_id) {
        let alert = this.alertCtrl.create({
            subTitle: 'How do you want to reorder these products?',
            buttons: [
                {
                    text: 'view details',
                    handler: () => {
                        /* this.service.addProductsToCart(this.orderItemsIDs)
                         .then((results) => this.handleAddProductsToCart(results));*/

                        /*     this.accountsService.emptyCart()
                                 .then((results) => {
                                     this.values.count= 0;
                                 })*/
                        this.openModal();
                    }
                },
                {
                    text: 'order again',
                    handler: () => {
                        /*this.service.duplicateOrder(order_id)
                            .then((results) => this.handleDuplicateOrder(results));*/
                        this.handleDuplicateOrderToCart();
                    }
                },

            ]
        });
        alert.present();
    }

    handleDuplicateOrder(results) {
        if (!results.success || results.success == false) {
            if (results.result == 'error') {
                this.functions.showAlertError("", results.msg);
            }
        } else if (results.success && results.success == true) {
            if (results.result == 'success') {
                this.functions.showAlertSuccess("", 'Your orders submitted successfully');
                this.filter.page = 1;
                this.service.getOrders(this.filter)
                    .then((results) => {
                        this.orders = results;
                        console.log(this.orders);
                    });
            }
        }
    }


    handleAddProductsToCart(results) {
        if (!results.success || results.success == false) {
            this.functions.showAlertError("", results.msg);
        } else if (results.success && results.success == true) {
            //this.functions.showAlertSuccess("", 'Your orders submitted successfully');
            this.getCart();
        }
    }


   async handleDuplicateOrderToCart() {
        let options: any = {};
        options.products_list = [];

        this.localCartService.emptyCart();
console.log(" this.orderItems",  this.orderItems)
        for (let i = 0; i < this.orderItems.length; i++) {
            if (this.orderItems[i].qty > 0) {
                options.products_list.push({
                    product_id: this.orderItems[i].product_id,
                    quantity: this.orderItems[i].qty
                });
                console.log("SSSS")
               await this.localCartService.addLocal(this.orderItems[i].product_id, this.orderItems[i].qty, false);
                // this.products[i].quantity = this.cart.cart_contents[j][1].quantity;
            }
        }
        console.log("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS")

        this.localCartService.calculate();
        // this.accountsService.emptyCart()
        //     .then((results) => {
        //         this.values.count = 0;
        //         this.service.addToCartBulk(options).then((results) => this.handleAddProductsToCart(results));
        //     });
    }


    /* addToCart(product_id){
     this.orderItemsIDs=[];
     this.orderItemsIDs.push(product_id);
     this.service.addProductsToCart(this.orderItemsIDs)
     .then((results) => this.handleAddProductsToCart(results));
     }*/

    addToCart(product) {
        this.product = product;
        this.options = [];
        this.options.product_id = this.product.product_id;
        if (this.values.isLoggedIn) {
            if (this.product.stock_amount != 0) {
                if (this.setVariations()) {
                    console.log(this.options)
                    this.productService.addToCart(this.options).then((results) => this.updateCart(results));
                }
            }

        } else {
            // this.showLoginToast();
        }

    }


    setVariations() {
        if (this.product.variationId != 0) {
            //this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
            this.options.variation_id = this.product.variationId;
            var variationName = this.get_keys_of_object(this.product.variationName);
            console.log("variationName: ", variationName)
            console.log("variationValue: ", this.product.variationName["'" + variationName + "'"])
            this.options['variation[attribute_pa_' + variationName[0] + ']'] = this.product.variationName["'" + variationName + "'"];

            /*   console.log("if setVariations")
             this.product.product.attributes.forEach(item => {
             if (item.selected) {
             this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
             }
             })
             for (var i = 0; i < this.product.product.attributes.length; i++) {
             if (this.product.product.attributes[i].variation && this.product.product.attributes[i].selected == undefined) {
             this.functions.showAlertWarning('', 'Please select product attribute.');
             return false;
             }
             }*/
            return true;
        } else {
            console.log("else setVariations")
            return true;
        }
    }


    updateCart(a) {
        if (!a.error) {
            this.showCartToast();
            this.values.count += parseInt(this.quantity);
        }
        /* this.disableSubmit = false;
         this.addToCartLoading = false;*/
    }


    showCartToast() {
        let toast = this.alertCtrl.create({
            message: 'Added to cart.',
            /* duration: 3000,
             position: 'bottom',
             //cssClass: 'rtl',
             showCloseButton: true,
             closeButtonText: "مشاهده سبد",
             closeButtonText: "view cart",*/
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(LocalCartPage);
            }
        });
        toast.present();
    }


    get_keys_of_object(my_object) {
        return Object.keys(my_object).map(function (key) {
            let pair = {};
            return key;
        });
    }


}
