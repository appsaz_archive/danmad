import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, AlertController, ToastController} from 'ionic-angular';
import {Values} from '../../../providers/service/values';
import {Functions} from '../../../providers/service/functions';
import {CartPage} from "../../cart/cart";
import {ProductService} from '../../../providers/service/product-service';
import {Service} from "../../../providers/service/service";
import {AccountsService} from "../../../providers/service/accounts-service";
import {LocalCartServiceProvider} from "../../../providers/service/local-cart-service";
import {LocalCartPage} from "../../local-cart/local-cart";


@IonicPage()
@Component({
    selector: 'page-reorder-modal',
    templateUrl: 'reorder-modal.html',
})
export class ReorderModalPage implements OnInit {

    orderItems: any;
    form: any;
    orderChanges: boolean = false;
    orderItemsIDs: any = [];
    product: any;
    options: any;
    reorderModal: any;
    count: any;
    offset: any;
    has_more_items: boolean = true;
    quantity: any;
    page: number = 1;
    id: any;
    filter: any;
    status: any;
    selectedItemIndex: number;
    disableBtn: boolean = false;
    AddedToCart: boolean = false;


    //noinspection TypeScriptUnresolvedVariable
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public nav: NavController,
                public alertCtrl: AlertController,
                public values: Values,
                public functions: Functions,
                public productService: ProductService,
                public service: Service,
                public accountsService: AccountsService,
                public localCartService: LocalCartServiceProvider,
                private toastCtrl: ToastController) {

        this.orderItems = this.navParams['data']['orderItems'];

        this.filter = {};
        this.filter.page = 1;
        this.count = 10;
        this.offset = 0;
        this.quantity = "1";
        this.filter.customer = this.service.loggedInCustomer.customer.id.toString();


        console.log(this.orderItems);
    }

    ngOnInit(): void {

    }


    getCart() {
        this.navCtrl.push(LocalCartPage);
    }


    closeModal() {
        this.viewCtrl.dismiss(false);
    }


    addToCart(product, index) {
        this.disableBtn = true;
        this.selectedItemIndex = index;
        if (this.AddedToCart == false) {
            //this.localCartService.emptyCart(); // no need to empty cart
            this.addToCartFunction(product);
            // this.emptyCart().then((results) => {
            //     this.addToCartFunction(product);
            // });
        } else {
            this.addToCartFunction(product);
        }


    }

    addToCartFunction(product) {
        this.product = product;
        this.options = [];
        this.options.product_id = this.product.product_id;
        if (this.values.isLoggedIn) {
            if (this.product.stock_amount != 0) {
                if (this.setVariations()) {
                    console.log(this.options)
                    this.localCartService.add(product.product_id, parseInt(this.quantity, 10), false, product.variationId).then(data => {
                        this.AddedToCart = true;
                        this.disableBtn = false;
                        // this.updateCart(data);
                    });
                    // this.productService.addToCart(this.options).then((results) => {
                    //     this.AddedToCart = true;
                    //     this.updateCart(results);
                    // });
                }
            }

        } else {
            // this.showLoginToast();
        }
    }


    setVariations() {
        if (this.product.variationId != 0) {
            //this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
            this.options.variation_id = this.product.variationId;
            let variationName = this.get_keys_of_object(this.product.variationName);
            console.log("variationName: ", variationName)
            console.log("variationValue: ", this.product.variationName["'" + variationName + "'"])
            this.options['variation[attribute_pa_' + variationName[0] + ']'] = this.product.variationName["'" + variationName + "'"];
            /*   console.log("if setVariations")
             this.product.product.attributes.forEach(item => {
             if (item.selected) {
             this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
             }
             })
             for (var i = 0; i < this.product.product.attributes.length; i++) {
             if (this.product.product.attributes[i].variation && this.product.product.attributes[i].selected == undefined) {
             this.functions.showAlertWarning('', 'Please select product attribute.');
             return false;
             }
             }*/
            return true;
        } else {
            console.log("else setVariations")
            return true;
        }
    }


    updateCart(a) {
        if (!a.error) {
            this.showCartToast();
            this.values.count += parseInt(this.quantity);
            this.disableBtn = false;
        }
        /* this.disableSubmit = false;
         this.addToCartLoading = false;*/
    }


    showCartToast() {
        let toast = this.toastCtrl.create({
            message: 'Added to cart.',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            closeButtonText: "view cart",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(LocalCartPage);
            }
        });
        toast.present();
    }


    get_keys_of_object(my_object) {
        return Object.keys(my_object).map(function (key) {
            let pair = {};
            return key;
        });
    }


    emptyCart() {
        return new Promise(resolve => {
            this.accountsService.emptyCart()
                .then((results) => {
                    this.values.count = 0;
                    resolve(1);
                })
        })
    }


}

