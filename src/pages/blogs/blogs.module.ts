import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogsPage } from './blogs';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(BlogsPage),
  ],
})
export class BlogsPageModule {}
