import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Network} from "@ionic-native/network";
import {Values} from "../../providers/service/values";
import {Service} from "../../providers/service/service";
import {Functions} from "../../providers/service/functions";
import {WishlistService} from "../../providers/service/wishlist-service";
import {BlogDetailPage} from "../blog-detail/blog-detail";
import {OfflinePage} from "../offline/offline";
import {LocalCartPage} from "../local-cart/local-cart";

/**
 * Generated class for the BlogsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-blogs',
    templateUrl: 'blogs.html',
})
export class BlogsPage {

    blogs: any = [];
    blog: any;
    loading: boolean = true;
    err: boolean = false;
    page: number = 1;
    hasMoreItems: boolean = true;

    constructor(private network: Network,
                public nav: NavController,
                public values: Values,
                public publicService: Service,
                public params: NavParams,
                public functions: Functions,
                public service: WishlistService) {


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BlogsPage');

        if (this.network.type == 'none') {
            this.nav.setRoot(OfflinePage);
        } else {

            this.publicService.getBlogs(this.page).then(data => {
                this.blogs = data;
                console.log("blogs loaded")
                this.loading = false;

            }, error => {
                this.err = true;
                this.loading = false;
            });
        }

    }


    loadData(event) {
        this.page = this.page + 1;
        this.publicService.getBlogs(this.page).then(data => {

            this.blogs.push.apply(this.blogs, data);

            if (data.length === 0) {
                this.hasMoreItems = false;
            }

        }, error => {


        })

    }

    getDetail(blog) {
        this.blog = blog;
        this.nav.push(BlogDetailPage, blog);
    }

    getCart() {
        this.nav.push(LocalCartPage);
    }

}
