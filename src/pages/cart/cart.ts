import {Component} from '@angular/core';
import {ModalController, NavController, NavParams, ToastController, AlertController} from 'ionic-angular';
import {CartService} from '../../providers/service/cart-service';
import {Values} from '../../providers/service/values';
import {Functions} from '../../providers/service/functions';
import {BillingAddressForm} from '../checkout/billing-address-form/billing-address-form';
import {Home} from '../home/home';
import {AccountLogin} from "../account/login/login";
import {AppsazSettings} from "../../appsazSettings";
import {CouponModalPage} from "../coupon-modal/coupon-modal";
import {ProductPage} from "../product/product";

@Component({
    templateUrl: 'cart.html'
})
export class CartPage {
    cart: any;
    status: any;
    obj: any;
    quantity: number;
    update: any;
    number: any;
    enableCheckout: boolean = true;
    coupon: any;
    res: any;
    disableSubmit: boolean = false;
    disableSubmitCoupon: boolean = false;
    chosen_shipping: any;
    shipping: any;
    cart_loading: boolean = true;
    couponModal: any;
    quantityLoading: boolean = false;
    quantityLoadingProductId: number = 0;

    constructor(public appsazSettings: AppsazSettings ,
                public nav: NavController,
                public modalCtrl: ModalController ,
                public service: CartService,
                public values: Values,
                public params: NavParams,
                public functions: Functions,
                private toastCtrl: ToastController,
                public alertCtrl: AlertController) {

        this.quantity = 1;
        this.obj = params.data;

        this.service.loadCart().then(results => {
                this.cart_loading = false;
                this.handleCartInit(results)
            },
            failure => {
            });

    }

    handleCartInit(results) {
        console.log("handleCartInit")
        console.log(results)
        this.cart = results;
        this.shipping = results.zone_shipping;
        this.chosen_shipping = results.chosen_shipping;
    }


    ionViewDidEnter(){
        this.enableCheckout = true;
    }


    handleCart(results) {
        this.cart = results;
        this.quantityLoading = false;

    }

    delete(id ,key) {
        let alert = this.alertCtrl.create({
            subTitle: 'Are you sure to remove this product from your shopping cart?',
            buttons: [
                {
                    text: 'Remove',
                    handler: () => {
                        this.quantityLoading = true;
                        this.quantityLoadingProductId = id;
                        this.service.deleteItem(key)
                            .then((results) => this.handleCart(results));
                    }
                },
                {
                    text: 'Cancel',
                    handler: () => {
                        return;
                    }
                }
            ]
        });
        alert.present();
    }


    checkout() {
        if (this.values.isLoggedIn && this.enableCheckout) {
            this.disableSubmit = true;
            this.enableCheckout = false;
            this.service.checkout()
                .then((results) => this.handleBilling(results));
        } else {
            this.showLoginToast()
        }

    }
    

    handleBilling(results) {
        this.disableSubmit = false;
        this.nav.push(BillingAddressForm, results);
    }

    deleteFromCart(id, key) {

        this.quantityLoading = true;
        this.quantityLoadingProductId = id;

        if (Object.keys(this.cart.cart_contents).length == 1) {
            if (this.cart.cart_contents[key].quantity == 1) {
                this.cart.cart_contents = {};
                console.log(this.cart);
            } ;
        }
        this.service.deleteFromCart(id, key)
            .then((results) => this.handleCart(results));

    }

    addToCart(id, key) {
        this.quantityLoading = true;
        this.quantityLoadingProductId = id;

        this.service.addToCart(id, key)
            .then((results) => this.handleCart(results));

    }

    submitCoupon() {
        if (this.cart.coupon != undefined) {
            this.disableSubmitCoupon = true;

            this.service.submitCoupon(this.cart.coupon)
                .then((results) => this.handleCoupon(results));
        }
    }

    removeCoupon() {
        this.service.removeCoupon(this.cart.applied_coupons)
            .then((results) => this.handleCoupon(results));
    }

    handleCoupon(results) {
        console.log(results);
        this.disableSubmitCoupon = false;
       /* if(results._body.index.indexOf("موفقیت") != -1){
            this.functions.showAlertSuccess("", "کد تخفیف با موفقیت اعمال شد.");
        }*/
        if(results._body.index.indexOf("موفقیت") != -1){
            this.functions.showAlertSuccess("", "Coupon submitted successfully.");
        }
        else {
            this.functions.showAlertWarning("", results._body);
        }
        this.service.loadCart()
            .then((results) => this.handleCart(results));
    }

    gohome() {
        this.nav.setRoot(Home);
    }

    showLoginToast() {
        let toast = this.toastCtrl.create({
            //message: 'لطفا به حساب کاربری خود وارد شوید.',
            message: 'Please login.',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
           // closeButtonText: "ورود"
            closeButtonText: "login"
        });

        toast.onDidDismiss((data, role) => {
            if (role== "close") {
                this.nav.push(AccountLogin);
            }
        });
        toast.present();
    }

    couponDetails(){

        this.couponModal = this.modalCtrl.create(CouponModalPage, { cart: this.cart });
        this.couponModal.present();

        this.couponModal.onDidDismiss(data => {
            this.cart = data;
            console.log("onDidDismiss data:");
            console.log(data);
        });

    }

    openItem(id){
        this.nav.push(ProductPage, id);
    }

}
