import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {CategoryService} from "../../providers/service/category-service";
import {CartService} from "../../providers/service/cart-service";
import {Functions} from "../../providers/service/functions";
import {LocalCartServiceProvider} from "../../providers/service/local-cart-service";

/**
 * Generated class for the FilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-coupon-modal',
    templateUrl: 'coupon-modal.html',
})
export class CouponModalPage {

    cart: any;
    disableSubmitCoupon: boolean = false;

    constructor(public navCtrl: NavController,
                public service: CartService,
                public navParams: NavParams,
                public catService: CategoryService,
                public publicService: Service,
                public viewCtrl: ViewController,
                public localCartServiceProvider: LocalCartServiceProvider,
                public functions: Functions) {

        console.log(this.navParams['data']['cart']);
        this.cart = this.navParams['data']['cart'];

    }


    // submitCoupon() {
    //     console.log("submit coupon")
    //     console.log(this.cart.coupon)
    //     if (this.cart.coupon != undefined) {
    //         this.disableSubmitCoupon = true;
    //         this.localCartServiceProvider.calculate(this.cart.coupon)
    //             .then((results) => this.handleCoupon(results));
    //     }
    // }



    submitCoupon() {
        console.log("submit coupon");
        console.log(this.cart.coupon);
        if (this.cart.coupon != undefined) {
            this.disableSubmitCoupon = true;
            // this.service.submitCoupon(this.cart.coupon)
            //     .then((results) => this.handleCoupon(results));
            this.localCartServiceProvider.calculate(this.cart.coupon)
                .then(results => {
                        this.cart = results;
                        if(!results['applied_coupons'] || results['applied_coupons'].length == 0){
                            console.log('coupon er');
                            console.log(results['applied_coupons'].length );
                            this.functions.showAlertError("", "invalid coupon");
                            this.disableSubmitCoupon = false;
                            this.cart.coupon ='';
                        } else {
                            this.functions.showAlertSuccess("", "coupon activated successfully.");
                        }
                        console.log('results copoun');
                        console.log(results);
                    }
                );
        }else{
            console.log('couponnnnnn');

        }
    }



    removeCoupon() {
        this.localCartServiceProvider.calculate(this.cart.applied_coupons)
            .then((results) => this.handleCoupon(results));
    }

    handleCoupon(results) {

        console.log("handleCoupon", results);

        this.cart = results;

        this.disableSubmitCoupon = false;
       /* if (results._body.indexOf("موفقیت") != -1) {
            this.functions.showAlertSuccess("", "کد تخفیف فعال شد.");
        } else if (results._body.indexOf("کوپن پاک شده است") != -1) {
            this.functions.showAlertSuccess("", "کد تخفیف حذف شد.");
            this.cart.coupon = "";
        } else if (results._body.indexOf("وجود ندارد") != -1) {
            this.functions.showAlertError("", "کد تخفیف وارد شده معتبر نمی باشد.");
            this.cart.coupon = "";
        } */


        // if (results._body.indexOf("موفقیت") != -1) {
        //     this.functions.showAlertSuccess("", "Coupon is active now.");
        // } else if (results._body.indexOf("کوپن پاک شده است") != -1) {
        //     this.functions.showAlertSuccess("", "Coupon removed!");
        //     this.cart.coupon = "";
        // } else if (results._body.indexOf("وجود ندارد") != -1) {
        //     this.functions.showAlertError("", "Coupon is not valid.");
        //     this.cart.coupon = "";
        // }
        // else {
        //     this.functions.showAlertWarning("", results._body);
        // }
        // this.service.loadCart()
        //     .then((results) => this.handleCart(results));
    }


    handleCart(results) {
        console.log("handleCart");
        this.cart = results;
    }

    closeModal() {
        this.viewCtrl.dismiss(this.cart);
    }

}
