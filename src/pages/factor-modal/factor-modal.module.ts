import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FactorModalPage } from './factor-modal';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(FactorModalPage),
  ],
})
export class FilterModalPageModule {}
