import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {HttpParams} from "@angular/common/http";
import {getBooleanPropertyValue} from "@ionic/app-scripts";
import {CategoryService} from "../../providers/service/category-service";

/**
 * Generated class for the FilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-factor-modal',
    templateUrl: 'factor-modal.html',
})
export class FactorModalPage implements OnInit {

    orderReview: any;
    form: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public catService: CategoryService,
                public service: Service,

                public viewCtrl: ViewController) {

        this.orderReview = this.navParams['data']['orderReview'];
        this.form = this.navParams['data']['form'];
        console.log("FactorModalPage");
        console.log(this.orderReview);
        console.log(this.orderReview.cart.cart_contents);
    }

    ngOnInit(): void {

    }


    closeModal() {
        this.viewCtrl.dismiss(false);
    }

}
