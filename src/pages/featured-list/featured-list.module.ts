import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {FeaturedListPage} from "./featured-list";


@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(FeaturedListPage),
  ],
})
export class FeaturedListPageModule {}
