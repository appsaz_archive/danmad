import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {Values} from "../../providers/service/values";
import {CartPage} from "../cart/cart";
import {ProductPage} from "../product/product";
import {Network} from "@ionic-native/network";
import {OfflinePage} from "../offline/offline";
import {AppsazSettings} from "../../appsazSettings";
import {FeaturedService} from "../../providers/service/featured-service";
import {LocalCartPage} from "../local-cart/local-cart";

/**
 * Generated class for the FeaturedListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-featured-list',
  templateUrl: 'featured-list.html',
})
export class FeaturedListPage {

    has_more_featured_items: boolean = true;
    featured_loading: boolean = true;

    constructor(public appsazSettings: AppsazSettings,
                private network: Network ,
                public params: NavParams ,
                public toastCtrl: ToastController,
                public nav: NavController,
                public service: Service,
                public values: Values,
                public featuredService : FeaturedService,
                ) {


        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }

        this.featuredService.getFeatured().then(sucess => {
                this.featured_loading = false;
            },
            failure => {
            });

    }

    getCart() {
        this.nav.push(LocalCartPage);
    }

    doInfinite_featured(infiniteScroll){
        console.log("doInfinite_featured");
        this.featuredService.loadMore_featured().then((results) => this.handleMoreFeatured(results, infiniteScroll));
    }

    handleMoreFeatured(results, infiniteScroll) {
        if (!results) {
            this.has_more_featured_items = false;
        }
        infiniteScroll.complete();
    }

    getProduct(item) {
        this.nav.push(ProductPage, item.id);
    }


}
