import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalCartPage } from './local-cart';

@NgModule({
  declarations: [
    // LocalCartPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalCartPage),
  ],
})
export class LocalCartPageModule {}
