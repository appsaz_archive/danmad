import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, AlertController, ToastController} from 'ionic-angular';
import {Cart, LocalCartServiceProvider} from "../../providers/service/local-cart-service";
import {CouponModalPage} from "../coupon-modal/coupon-modal";
import {CheckoutService} from "../../providers/service/checkout-service";
import {CartService} from "../../providers/service/cart-service";
import {BillingAddressForm} from "../checkout/billing-address-form/billing-address-form";
import {Functions} from "../../providers/service/functions";
import {Plugins} from "@capacitor/core";
import {Values} from "../../providers/service/values";
import {AccountLogin} from "../account/login/login";
import {Home} from "../home/home";
import {ProductPage} from "../product/product";
import {AppsazSettings} from "../../appsazSettings";
import {logoDesignernews} from "ionicons/icons";

const {Storage} = Plugins;

/**
 * Generated class for the LocalCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-local-cart',
    templateUrl: 'local-cart.html',
})
export class LocalCartPage implements OnInit {
    cart: Cart;
    couponModal: any;
    disableSubmit: boolean = false;
    buttonCoupon: boolean = false;
    disableSubmitCoupon: boolean = false;
    coupon: any;
    Checkout: any;
    Apply: any;

    enableCheckout: boolean = true;

    quantityLoading: boolean = false;
    quantityLoadingProductId: number = 0;

    constructor(public navCtrl: NavController,
                public appsazSettings: AppsazSettings,
                public navParams: NavParams,
                public localCartServiceProvider: LocalCartServiceProvider,
                public modalCtrl: ModalController,
                public checkoutservice: CheckoutService,
                public service: CartService,
                public functions: Functions,
                private toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public values: Values) {
        this.Checkout = "Checkout";
        this.Apply = "Apply";
        // this.localCartServiceProvider.calculate()
        //     .then(cart => {
        //             this.cart = cart;
        //             console.log('local cart');
        //             console.log(this.cart);
        //             console.log(this.cart.subtotal);
        //             console.log(this.cart.items);
        //         }
        //     );
    }

    ngOnInit() {
        // this.localCartServiceProvider.calculate()
        //     .then(cart => {
        //             this.cart = cart;
        //             console.log('local cart');
        //             console.log(this.cart);
        //             console.log(this.cart.subtotal);
        //             console.log(this.cart.items);
        //         }
        //     );
        // this.localCartServiceProvider.cartWatcher
        //     .subscribe(cart => this.cart = cart);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LocalCartPage');
    }

    ionViewWillEnter() {
        this.enableCheckout = true;
        this.localCartServiceProvider.calculate()
            .then(cart => {
                    this.cart = cart;
                    console.log('local cart');
                    console.log(this.cart);
                    console.log(this.cart.subtotal);
                    console.log(this.cart.items);
                }
            );
    }

    couponDetails() {
        console.dir(this.cart);

        this.couponModal = this.modalCtrl.create(CouponModalPage, {cart: this.cart});
        this.couponModal.present();

        this.couponModal.onDidDismiss(data => {
            this.cart = data;
            console.log("onDidDismiss data:");
            console.log();
        });

    }

    // checkout() {
    //     this.disableSubmit = true;
    //     this.Checkout = "PleaseWait";
    //     // this.service.checkout()
    //     //     .then((results) => {
    //     //         console.log(results);
    //     //         this.handleBilling(results)
    //     //     });
    //
    //     this.localCartServiceProvider.calculate(this.cart.coupons[0]).then(results => {
    //             console.log('local cart calculate');
    //             console.log(results);
    //             this.disableSubmit = false;
    //             this.Checkout = "Checkout";
    //             this.navCtrl.push(BillingAddressForm, {results: results, needAddress: false, coupon: this.cart.coupons[0]});
    //         }
    //     );
    // }
    //
    // handleBilling(results) {
    //     this.disableSubmit = false;
    //     this.Checkout = "Checkout";
    //     console.log('handlebillingggg');
    //     console.log(results);
    //     // if (this.needAddress) {
    //     //     // this.nav.push(BillingAddressForm, results);
    //     //     this.navCtrl.push(BillingAddressForm, {results:results,needAddress:false});
    //     // } else {
    //     // this.fillForm();
    //     // this.checkoutservice.checkout(this.form)
    //     //     .then((results) => this.handlechPayment(results));
    //     this.navCtrl.push(BillingAddressForm, {results: results, needAddress: false, coupon: this.cart.coupons[0]});
    //     // }
    //
    // }

    checkout() {
        if (this.values.isLoggedIn && this.enableCheckout) {
            this.disableSubmit = true;
            this.enableCheckout = false;


            console.log(this.cart);

            let coupon = null;

            if (this.cart['applied_coupons'].length > 0) {
                coupon = this.cart['applied_coupons'][0];
            }

            this.localCartServiceProvider.calculate(coupon).then(results => {
                    console.log('local cart calculate', results);
                    this.disableSubmit = false;
                    this.Checkout = "Checkout";

                    let coupon = null;
                    if (this.cart['applied_coupons']) {
                        if (this.cart['applied_coupons'].length > 0) {
                            coupon = this.cart['applied_coupons'][0];
                        }
                    }

                    this.navCtrl.push(BillingAddressForm,
                        {
                            results: results,
                            needAddress: false,
                            coupon: coupon
                        }
                    );
                }
            );

            // this.service.checkout()
            //     .then((results) => {
            //         console.log('this.service.checkout', results);
            //         this.disableSubmit = false;
            //         this.navCtrl.push(BillingAddressForm, results);
            //     });
        } else {
            this.showLoginToast();
        }

    }


    handleBilling(results) {
        this.disableSubmit = false;
        this.navCtrl.push(BillingAddressForm, results);
    }

    showLoginToast() {
        let toast = this.toastCtrl.create({
            //message: 'لطفا به حساب کاربری خود وارد شوید.',
            message: 'Please login.',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            // closeButtonText: "ورود"
            closeButtonText: "login"
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.navCtrl.push(AccountLogin);
            }
        });
        toast.present();
    }

    removeCoupon() {
        // this.service.removeCoupon(this.cart.coupons[0])
        //     .then((results) => this.handleCoupon(results));
        this.localCartServiceProvider.remove_coupon(this.cart['applied_coupons'][0])
            .then(results => {
                    //             // this.cart = results;
                    //             console.log('results coupon remove');
                    //             console.log(results);

                    this.localCartServiceProvider.calculate()
                        .then(results => {
                                this.cart = results;
                                this.functions.showAlertSuccess("", "کد تخفیف حذف شد.");
                                console.log('results coupon calc');
                                console.log(results);
                            }
                        );
                }
            );
    }

    handleCoupon(results) {
        console.log(results);
        this.disableSubmitCoupon = false;
        this.Apply = "Apply";
        this.functions.showAlertMessage("status", results._body);
        this.service.loadCart()
            .then((results) => this.handleCart(results));
    }

    handleCart(results) {
        this.cart = results;
        console.log(results);

    }

    gohome() {
        this.navCtrl.setRoot(Home);
    }

    openItem(id) {
        this.navCtrl.push(ProductPage, id);
    }

    deleteFromCart(key) {
        console.log('cart itemsssss');
        console.log(this.cart.items);

        this.quantityLoading = true;
        this.quantityLoadingProductId = key;

        Storage.get({key: 'cart'}).then(cartData => {
            console.log("capacitor cartData", cartData);
            var data = JSON.parse(cartData.value);
            if (data != null) {
                console.log('jjjjj', data);
                var index = data.map(function (o) {
                    return o.product_id;
                }).indexOf(key);
                console.log('indexxxx:', index);
                if (index != -1) {
                    if (data[index].quantity == 1) {
                        data.splice(index, 1);
                    } else {
                        data[index].quantity--;
                    }
                }
                console.dir(data);
                Storage.set({key: 'cart', value: JSON.stringify(data)});
                this.values.count = data.length;
                this.localCartServiceProvider.calculate2(data)
                    .then(cart => {
                            this.cart = cart;
                            this.quantityLoading = false;
                        }
                    );
            }
        }, error => {
            console.error('cart error');
            console.error(error);
            this.quantityLoading = false;
        });

    }

    addToCart(product_id) {
        console.log('cart items');
        console.log(this.cart.items);

        this.quantityLoading = true;
        this.quantityLoadingProductId = product_id;

        Storage.get({key: 'cart'}).then(cartData => {
            console.log("capacitor cartData");
            let data = JSON.parse(cartData.value);
            if (data != null) {
                console.log('jjjjj', data);
                let index = data.map((o) => {
                    return o.product_id;
                }).indexOf(product_id);
                console.log('index', index);

                if (index != -1) {
                    data[index].quantity++;
                } else {
                    data.push({product_id: product_id, quantity: 1})
                }
                console.log(data);
                Storage.set({key: 'cart', value: JSON.stringify(data)});
                this.values.count = data.length;
                this.localCartServiceProvider.calculate2(data)
                    .then(cart => {
                            this.cart = cart;
                            this.quantityLoading = false;
                        }
                    ).catch(err => {
                    console.dir(err);

                    let toast = this.toastCtrl.create({
                        message: 'Please wait 4 seconds!',
                        duration: 4000,
                        position: 'bottom',
                        cssClass: 'rtl',
                        showCloseButton: false,
                    });
                    toast.present();

                    setTimeout(() => {
                        this.quantityLoading = false;
                    }, 4000);
                });
            }
        }, error => {
            console.error('cart error', error);
            this.quantityLoading = false;
        });
    }

    emptyCart(product_id) {
        let alert = this.alertCtrl.create({
            title: '',
            subTitle: 'Are you sure to remove this product from your shopping cart?',
            buttons: [
                {
                    text: 'Remove',
                    handler: () => {
                        console.log('cart itemsssss', this.cart.items);

                        this.quantityLoading = true;
                        this.quantityLoadingProductId = product_id;

                        Storage.get({key: 'cart'}).then(cartData => {
                            console.log("capacitor cartData");
                            var data = JSON.parse(cartData.value);
                            if (data != null) {
                                console.log('jjjjj');
                                console.log(data);

                                var index = data.map(function (o) {
                                    return o.product_id;
                                }).indexOf(product_id);

                                data.splice(index, 1);

                                Storage.set({key: 'cart', value: JSON.stringify(data)});
                                this.values.count = data.length;
                                this.localCartServiceProvider.calculate2(data)
                                    .then(cart => {
                                            this.cart = cart;
                                            this.quantityLoading = false;
                                        }
                                    );
                            }
                        }, error => {
                            console.error('cart error');
                            console.error(error);
                            this.quantityLoading = false;
                        });
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        return;
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        alert.present();
    }
}
