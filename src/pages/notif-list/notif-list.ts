import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {notificationService} from "../../providers/service/notification-service";
import {OfflinePage} from "../offline/offline";
import {Network} from "@ionic-native/network";

/**
 * Generated class for the NotifListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-notif-list',
    templateUrl: 'notif-list.html',
})
export class NotifListPage {

    loadingMessages: boolean = true;
    noMessages: boolean = false;
    messages: any;
    offset: number = 0;
    limit: number = 10;
    has_more_messages =true;
    constructor(private network: Network,
                public navCtrl: NavController,
                public navParams: NavParams,
                public service: Service,
                public notifService: notificationService) {

        if (this.network.type === 'none') {
            this.navCtrl.setRoot(OfflinePage);
        }


    }

    ionViewDidLoad() {
        this.notifService.getMessages(this.offset, this.limit).then(data => {
            this.messages = data;
            this.loadingMessages = false;

            for (let i = 0; i <data.length ; i++) {
                if(data[i].seen == 0){
                    this.notifService.setSeen(data [i].messageId);
                }

            }


        }, err => {
            this.noMessages = true;
            this.loadingMessages = false;
        });

    }


    doInfinite_messages(infiniteScroll) {
        this.offset = this.offset + this.limit;
        this.notifService.getMessages(this.offset, this.limit).then(data => {

            if(data == []){
                console.log('finished!');
               this. has_more_messages =false;
                infiniteScroll.complete();

            }
            else{
                for (let i = 0; i <data.length ; i++) {
                    this.messages.push(data[i]);
                }

                for (let i = 0; i <data.length ; i++) {
                    if(data[i].seen == 0){
                        this.notifService.setSeen(data [i].messageId);
                    }

                }
            }
        });

        infiniteScroll.complete();

       }

}
