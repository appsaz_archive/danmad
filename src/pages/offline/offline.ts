import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';

/**
 * Generated class for the OfflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-offline',
    templateUrl: 'offline.html',
})
export class OfflinePage {

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public platform: Platform) {
    }

    exitApp() {
        this.platform.exitApp();
    }

    reload() {
        window.location.reload();
    }

}
