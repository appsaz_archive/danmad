import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnSaleListPage } from './on-sale-list';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(OnSaleListPage),
  ],
})
export class OnSaleListPageModule {}
