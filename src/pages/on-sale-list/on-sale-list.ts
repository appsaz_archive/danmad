import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Service} from "../../providers/service/service";
import {Values} from "../../providers/service/values";
import {Functions} from "../../providers/service/functions";
import {CartPage} from "../cart/cart";
import {ProductPage} from "../product/product";
import {Network} from "@ionic-native/network";
import {OfflinePage} from "../offline/offline";
import {AppsazSettings} from "../../appsazSettings";
import {OnsaleService} from "../../providers/service/onsale-service";
import {LocalCartPage} from "../local-cart/local-cart";

/**
 * Generated class for the OnSaleListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-on-sale-list',
  templateUrl: 'on-sale-list.html',
})
export class OnSaleListPage {

    has_more_onsale_items: boolean = true;
    on_sale_loading: boolean = true;


    constructor(public appsazSettings: AppsazSettings,
                private network: Network ,
                public params: NavParams ,
                public toastCtrl: ToastController,
                public nav: NavController,
                public service: Service,
                public values: Values,
                public onsaleService : OnsaleService
                ) {


        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }

        this.onsaleService.getOnsale().then(sucess => {
                this.on_sale_loading = false;
            },
            failure => {
            });

    }

    getCart() {
        this.nav.push(LocalCartPage);
    }


    doInfinite_onsale(infiniteScroll){
        console.log("doInfinite_onsale");
        this.onsaleService.loadMore_onsale().then((results) => this.handleMoreOnsale(results, infiniteScroll));
    }


    handleMoreOnsale(results, infiniteScroll) {
        if (!results) {
            this.has_more_onsale_items = false;
        }
        infiniteScroll.complete();
    }


    getProduct(item) {
        this.nav.push(ProductPage, item.id);
    }


}
