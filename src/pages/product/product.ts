import {Component, ViewChild} from '@angular/core';
import {
    NavController,
    NavParams,
    Content,
    ToastController,
    Slides,
    ModalController
} from 'ionic-angular';
import {ProductService} from '../../providers/service/product-service';
import {Values} from '../../providers/service/values';
import {Functions} from '../../providers/service/functions';
import {md5} from './md5';
import {CartPage} from '../cart/cart';
import {ReviewsPage} from "../reviews/reviews";
import {SocialSharing} from "@ionic-native/social-sharing";
import {Service} from "../../providers/service/service";
import {AppsazSettings} from "../../appsazSettings";
import {AccountLogin} from "../account/login/login";
import {OfflinePage} from "../offline/offline";
import {Network} from "@ionic-native/network";
import {GalleryModal} from 'ionic-gallery-modal';
import {animate, style, transition, trigger} from "@angular/animations";
import {LocalCartServiceProvider} from "../../providers/service/local-cart-service";
import {LocalCartPage} from "../local-cart/local-cart";


@Component({
    animations: [
        trigger(
            'myBlockAnimation',
            [
                transition(
                    ':enter', [
                        style({transform: 'translateX(100%)', opacity: 0}),
                        animate('500ms', style({transform: 'translateX(0)', 'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({transform: 'translateX(0)', 'opacity': 1}),
                        animate('500ms', style({transform: 'translateX(100%)', 'opacity': 0}))

                    ]
                )]
        ),
        trigger(
            'myBoxAnimation',
            [
                transition(
                    ':enter', [
                        style({opacity: 0}),
                        animate('500ms', style({'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({'opacity': 1}),
                        animate('500ms', style({'opacity': 0}))

                    ]
                )]
        )
    ],
    templateUrl: 'product.html'
})
export class ProductPage {
    @ViewChild(Content) content: Content;

    product: any = {};
    relatedLoading: boolean = true;
    relatedFilter: any =[];
    id: any;
    related: any = [];
    status: any;
    options: any;
    message: any;
    quantity: any;
    reviews: any;
    disableSubmit: boolean = false;
    loading: boolean = true;
    usedVariationAttributes: any = [];
    variationsSet: boolean = false;
    addToCartLoading: boolean = false;


    @ViewChild(Slides) private slides: Slides;
    private photos: any[] = [];

    constructor(public appsazSettings: AppsazSettings,
                public modalCtrl: ModalController,
                public nav: NavController,
                public service: ProductService,
                public publicService: Service,
                public params: NavParams,
                private network: Network,
                public functions: Functions,
                public values: Values,
                private toastCtrl: ToastController,
                public localCartService: LocalCartServiceProvider,
                private socialSharing: SocialSharing) {


        this.options = [];
        this.quantity = "1";
        this.id = params.data;

        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }
        
        this.addPostViews();

        this.service.getProduct(params.data)
            .then((results) => {
                    this.handleProductResults(results)

                    this.service.getPostViews(this.id)
                        .then((results) => this.handlePostViewsResults(results));

                }
            );

        

    }

    ionViewDidEnter() {
        // this.slides.dir= 'rtl';
    }

    handleProductResults(results) {
        this.product = results;
        this.options.product_id = this.id;
        this.createPhotos();
        this.getRelatedProducts();
        this.loading = false;

        this.usedVariationAttributes = this.product.product.attributes.filter(function (attribute) {
            return attribute.variation == true
        });
        this.variationsSet = this.checkVariations();

        if (this.product.product.type == 'simple') {
            this.variationsSet = true;
        }

    }

    getProduct(id) {
        this.nav.push(ProductPage, id);
    }

    addToCart() {

        if(this.appsazSettings.addtocartAsGuest){
            if (this.product.product.in_stock) {
                if (this.setVariations()) {
                    this.addToCartLoading = true;
                    this.service.addToCart(this.options).then((results) => this.updateCart(results));
                }
            }
        }
        else{
            if (this.values.isLoggedIn) {

                if (this.product.product.in_stock) {
                    if (this.setVariations()) {
                        console.log(this.options)
                        this.service.addToCart(this.options).then((results) => this.updateCart(results));
                    }
                }

            } else {
                this.showLoginToast();
            }
        }

    }


    setVariations() {
        if(this.product.product.type == "simple"){
            return true;
        }
        else if (this.product.product.attributes) {
            console.log("if setVariations")

            this.product.product.attributes.forEach(item => {
                if (item.selected) {
                    this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
                }
            })
            for (var i = 0; i < this.product.product.attributes.length; i++) {
                if (this.product.product.attributes[i].variation && this.product.product.attributes[i].selected == undefined) {

                    /*this.functions.showAlertWarning('', ' لطفا همه ی ویژگی های سفارش خود را انتخاب کنید');*/
                    this.functions.showAlertWarning('', 'Please select product attribute.');
                    return false;
                }
            }
            return true;
        } else {
            console.log("else setVariations")
            return true;
        }

    }


    checkVariations() {
        if (this.product.product.attributes) {
            this.product.product.attributes.forEach(item => {
                if (item.selected) {
                    this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
                }
            })
            for (var i = 0; i < this.product.product.attributes.length; i++) {
                if (this.product.product.attributes[i].variation && this.product.product.attributes[i].selected == undefined) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    updateCart(a) {
        if(!a.error){
            this.showCartToast();
            this.values.count += parseInt(this.quantity);
        }
        this.disableSubmit = false;
        this.addToCartLoading = false;
    }


    showCartToast() {
        let toast = this.toastCtrl.create({
            /* message: 'به سبد خرید اضافه شد.',*/
            message: 'Added to cart.',
            duration: 5000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            /*closeButtonText: "مشاهده سبد",*/
            closeButtonText: "view cart",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(LocalCartPage);
            }
        });
        toast.present();
    }

    getCart() {
        this.nav.push(LocalCartPage);
    }

    addToWishlist(id) {
        if (this.values.isLoggedIn) {
            this.service.addToWishlist(id).then((results) => {
                this.update(results);
                console.log(results)
            });
        } else {
            this.showLoginToast();
        }
    }

    update(a) {
        if (a.success == "Success") {
            this.values.wishlistId[this.product.product.id] = true;
        } else {
            /* this.functions.showToast("خطا");*/
            this.functions.showToast("Error");
        }
    }

    removeFromWishlist(id) {
        this.values.wishlistId[id] = false;
        this.service.deleteItem(id).then((results) => this.updateWish(results, id));
    }

    updateWish(results, id) {
        if (results.status == "success") {
            this.values.wishlistId[id] = false;
        }
    }

    chooseVariation(att, value) {

        this.variationsSet = this.checkVariations();

        this.product.product.attributes.forEach(item => {
            if (item.name == att.name) {
                item.selected = value;
            }
        })
        this.product.product.variations.forEach(variation => {

            var test = new Array(this.usedVariationAttributes.length);
            test.fill(false);
            this.usedVariationAttributes.forEach(attribute => {

                if (variation.attributes.length == 0) {

                    this.product.product.in_stock = variation.in_stock;
                    this.product.product.price = variation.price;
                    this.product.product.sale_price = variation.sale_price;
                    this.product.product.regular_price = variation.regular_price;

                } else {

                    variation.attributes.forEach((item, index) => {

                        if (attribute.selected &&
                            item.name.toUpperCase() == attribute.name.toUpperCase() &&
                            item.option.toUpperCase() == attribute.selected.toUpperCase()) {

                            test[index] = true;
                            this.variationsSet = this.checkVariations();
                        }
                    });
                    if (test.every(v => v == true)) {
                        this.options.variation_id = variation.id;
                        this.product.product.on_sale = variation.on_sale;
                        this.product.product.price = variation.price;
                        this.product.product.in_stock = variation.in_stock;
                        this.product.product.sale_price = variation.sale_price;
                        this.product.product.regular_price = variation.regular_price;

                        test.fill(false);
                    }
                    ;
                }

            })
        })
    }

    openReviews() {
        console.log(this.id)
        console.log(this.reviews)
        this.nav.push(ReviewsPage, {id: this.id, reviews: this.reviews});
    }

    shareItem(item) {

        /* var msg = this.product.product.title;
         msg = msg + " را در ‏« ";
         msg = msg + this.appsazSettings.appNameFa;
         msg = msg + " » بینید: ";
         msg = msg + " \n ";
         msg = msg + this.appsazSettings.serverURL + "/DeepLink.php?id=" + item.id;
         msg = msg + " \n ";
         msg = msg + " و یا « ";
         msg = msg + this.appsazSettings.appNameFa;
         msg = msg + " » را از لینک زیر دانلود کنید: ";
         msg = msg + " \n ";
         msg = msg + this.appsazSettings.shareProductDownloadLink;*/

        var msg = "View";
        msg = msg + this.product.product.title;
        msg = msg + "at ' ";
        msg = msg + this.appsazSettings.appNameFa;
        msg = msg + " '";
        msg = msg + " \n ";
        msg = msg + this.appsazSettings.serverURL + "/DeepLink.php?id=" + item.id;
        msg = msg + " \n ";
        msg = msg + " or download '";
        msg = msg + this.appsazSettings.appNameFa;
        msg = msg + " ' from below ";
        msg = msg + " \n ";
        msg = msg + this.appsazSettings.shareProductDownloadLink;


        this.socialSharing.share(msg)
            .then(() => {
            })
            .catch(() => {
            });
    }

    getRelatedProducts() {
        if (this.product.product.cross_sell_ids.length) {
            for (let item in this.product.product.cross_sell_ids)
                this.relatedFilter['include[' + item + ']'] = this.product.product.cross_sell_ids[
                    item
                    ];

            console.log(this.relatedFilter);

            this.service
                .getRelatedProducts(this.relatedFilter)
                .then(results =>{
                    this.relatedLoading = false;
                    this.related = results

                });
        }
    }

    showLoginToast() {
        let toast = this.toastCtrl.create({
            /*message: 'لطفا به حساب کاربری خود وارد شوید.',*/
            message: 'Please login',
            duration: 2000,
            position: 'bottom',
            cssClass: 'rtl',
            showCloseButton: true,
            /*closeButtonText: "ورود",*/
            closeButtonText: "login",
        });

        toast.onDidDismiss((data, role) => {
            if (role == "close") {
                this.nav.push(AccountLogin);
            }
        });
        toast.present();
    }


    private openModal(initialSlide) {
        let modal = this.modalCtrl.create(GalleryModal, {
            photos: this.photos,
            initialSlide: initialSlide, // The second image
        });
        modal.present();
    }


    private createPhotos() {
        for (let i = 0; i < this.product.product.images.length; i++) {
            this.photos.push({
                url: this.product.product.images[i].src,
            });
        }
    }
    
    handlePostViewsResults(results) {
        this.product.product.views = results;
        console.log("this.product.views: ", this.product.product.views)
        this.loading = false;
    }


    addPostViews(){
        this.service.addPostViews(this.id)
            .then((results) => this.handleAddPostViewsResults(results));
    }

    handleAddPostViewsResults(results){
        
    }

}
