import {Component} from '@angular/core';
import {NavController, NavParams, PopoverController, ModalController, AlertController} from 'ionic-angular';
import {CategoryService} from '../../providers/service/category-service';
import {Values} from '../../providers/service/values';
import {Functions} from '../../providers/service/functions';
import {CartPage} from '../cart/cart';
import {ProductPage} from '../product/product';
import {Service} from "../../providers/service/service";
import {FilterModalPage} from "../filter-modal/filter-modal";
import {OfflinePage} from "../offline/offline";
import {Network} from "@ionic-native/network";
import {AppsazSettings} from "../../appsazSettings";
import {animate, style, transition, trigger} from "@angular/animations";
import {AccountsService} from "../../providers/service/accounts-service";
import {LocalCartPage} from "../local-cart/local-cart";

@Component({

    animations: [
        trigger(
            'myBlockAnimation',
            [
                transition(
                    ':enter', [
                        style({transform: 'translateX(100%)', opacity: 0}),
                        animate('500ms', style({transform: 'translateX(0)', 'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({transform: 'translateX(0)', 'opacity': 1}),
                        animate('500ms', style({transform: 'translateX(100%)', 'opacity': 0}))

                    ]
                )]
        ),
        trigger(
            'myBoxAnimation',
            [
                transition(
                    ':enter', [
                        style({opacity: 0}),
                        animate('500ms', style({'opacity': 1}))
                    ]
                ),
                transition(
                    ':leave', [
                        style({'opacity': 1}),
                        animate('500ms', style({'opacity': 0}))

                    ]
                )]
        )
    ],
    selector: 'page-products',
    templateUrl: 'products.html'
})
export class ProductsPage {
    products: any;
    count: any;
    offset: any;
    category: any;
    has_more_items: boolean = true;
    status: any;
    options: any;
    categories: any;
    subCategories: any;
    items: any;
    quantity: any;
    q: any;
    showFilters: boolean = false;
    data: any;
    sort: number = 0;
    categoryName: any;
    title: boolean;
    filterModal: any;
    sortValue: number = 2;
    sortValueArray: any = [false, false, true, false];
    firstPage: boolean = true;
    pageParams: any;
    filter_types: any = false;
    selectedBrand: any;
    selectedCategory: any;
    loading: boolean= true;

    constructor(public appsazSettings : AppsazSettings,
                public nav: NavController,
                public popoverCtrl: PopoverController,
                public service: CategoryService,
                public publicService: Service,
                private network: Network ,
                params: NavParams,
                public values: Values,
                public functions: Functions,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController) {

        this.data = {};
        this.service.globalFilter = {};
        this.q = "";
        this.pageParams = params;
        this.count = 10;
        this.offset = 0;
        this.values.filter = {};
        this.options = [];
        this.subCategories = [];
        this.items = [];
        this.quantity = "1";
        this.products = null;
        this.has_more_items = true;


        if (this.network.type === 'none') {
            this.nav.setRoot(OfflinePage);
        }

        this.publicService.filter_types = [];
        this.getData(this.pageParams);

    }



    getData(params) {
        if (params.data.id == 0 && params.data.name == 0) {
            this.firstPage = true;
            this.title = false;
            this.subCategories = this.service.mainCategories;
            this.products = this.service.getProducts(this.service.globalFilter).then(data => {
                this.products = data;
                this.loading= false;
            });

        } else {
            this.firstPage = false;
            this.title = true;
            this.service.globalFilter['category' ] = params.data['category']['id'];
            this.categoryName = params.data['category']['name'];
            this.selectedCategory=params.data;
            if (this.selectedCategory['category']['categories']) {
                this.subCategories = this.selectedCategory['category']['categories'];

                this.chnageFilter(this.sortValue);

            } else {
                this.subCategories = params;
                this.loading= false;
            }
        }
    }

    getCategory(id, slug, name) {

        this.items.id = id;
        this.items.slug = slug;
        this.items.name = name;
        this.items.categories = this.categories;
        this.service.globalFilter['category' ] = id;
        this.items.categories = this.service.categories.filter(item => item.parent === parseInt(id));


        this.nav.push(ProductsPage, {'category': this.items});
    }


    parseText(id, count, offset, obj2) {
        var text = '{';
        text += '"category' + '":"' + id + '"}';
        var obj1 = JSON.parse(text);
        var obj3 = {};
        for (var attrname in obj1) {
            obj3[attrname] = obj1[attrname];
        }
        for (attrname in obj2) {
            obj3[attrname] = obj2[attrname];
        }
        return obj3;
    }

    getProducts(item) {
        this.nav.push(ProductsPage, item);
    }

    getProduct(id) {
        this.nav.push(ProductPage, id.id);
    }

    getCart() {
        this.nav.push(LocalCartPage);
    }

    doInfinite(infiniteScroll) {
        this.service.globalFilter.page += 1;
        this.service.loadMore(this.service.globalFilter).then((results) => this.handleMore(results, infiniteScroll));
    }

    handleMore(results, infiniteScroll) {
        if (results != undefined) {
            for (var i = 0; i < results.length; i++) {
                this.products.push(results[i]);


            }
            ;
        }
        if (results.length == 0) {
            this.has_more_items = false;
        }
        infiniteScroll.complete();
    }


    deleteFromCart(id) {
        this.service.deleteFromCart(id).then((results) => this.status = results);
    }

    updateToCart(id) {
        this.service.updateToCart(id).then((results) => this.status = results);
    }

    addToCart(id, type) {
        if (type == 'variable') {
            this.nav.push(ProductPage, id);
        } else {
            var text = '{';
            var i;
            for (i = 0; i < this.options.length; i++) {
                var res = this.options[i].split(":");
                text += '"' + res[0] + '":"' + res[1] + '",';
            }
            text += '"product_id":"' + id + '",';
            text += '"quantity":"' + this.quantity + '"}';
            var obj = JSON.parse(text);
            this.service.addToCart(obj).then((results) => this.updateCart(results));
        }
    }

    updateCart(a) {
    }

    onInput($event) {

        this.service.globalFilter.page = 1;
        this.service.globalFilter.search = $event.srcElement.value;
        this.service.search(this.service.globalFilter).then((results) => this.handleSearchResults(results));

    }

    handleSearchResults(results) {
        this.products = results;


    }

    onCancel($event) {
        console.log('cancelled');
    }

    getFilter() {
        this.showFilters = true;
        this.has_more_items = false;
    }

    cancel() {
        this.showFilters = false;
        this.has_more_items = true;
    }

    chnageFilter(sort) {
        this.showFilters = false;
        this.has_more_items = true;
        this.service.globalFilter.page = 1;
        if (sort == 2) {
            this.service.globalFilter['order'] = "desc";
            this.service.globalFilter['orderby'] = "date";
        } else if (sort == 11) {
            this.service.globalFilter['order'] = "asc";
            this.service.globalFilter['orderby'] = "title";
        } else if (sort == 10) {
            this.service.globalFilter['order'] = "desc";
            this.service.globalFilter['orderby'] = "title";
        } else if (sort == 0) {
            this.service.globalFilter['order'] = "asc";
            this.service.globalFilter['orderby'] = "price";
        } else if (sort == 1) {
            this.service.globalFilter['order'] = "desc";
            this.service.globalFilter['orderby'] = "price";
        } else if (sort == 3) {
            this.service.globalFilter['order'] = "desc";
            this.service.globalFilter['orderby'] = "total_views";
        }

        this.service.load(this.service.globalFilter).then((results) =>{
            this.products = results;
            this.loading= false;
        } );
    }

    addToWishlist(id) {
        if (this.values.isLoggedIn) {
            this.values.wishlistId[id] = true;
            this.service.addToWishlist(id).then((results) => this.update(results, id));
        } else {
            this.functions.showAlertWarning("Warning", "You must login to add product to wishlist");
        }
    }

    update(results, id) {
        if (results.success == "Success") {
            this.values.wishlistId[id] = true;
        } else {
        }
    }

    removeFromWishlist(id) {
        this.values.wishlistId[id] = false;
        this.service.deleteItem(id).then((results) => this.updateWish(results, id));
    }

    updateWish(results, id) {
        if (results.status == "success") {
            this.values.wishlistId[id] = false;
        }
    }

    openFilters() {
        this.filterModal = this.modalCtrl.create(FilterModalPage);
        this.filterModal.onDidDismiss(data => {
            if (data != false) {
                this.products = data.data;
            }
        });

        this.filterModal.present();

    }


    sortBy() {

        let alert = this.alertCtrl.create({
            /*title: 'مرتب سازی بر اساس:',*/
            title: 'Sort by:',
            inputs: [
                {
                    type: 'radio',
                    /*label: 'قیمت کم به زیاد',*/
                    label: 'Price(ASC)',
                    value: '0',
                    checked: this.sortValueArray[0]
                },
                {
                    type: 'radio',
                    /*label: 'قیمت زیاد به کم',*/
                    label: 'Price(DESC)',
                    value: '1',
                    checked: this.sortValueArray[1]
                },
                {
                    type: 'radio',
                    /*label: 'جدیدترین',*/
                    label: 'Newest',
                    value: '2',
                    checked: this.sortValueArray[2]
                },

                {
                    type: 'radio',
                    /*label: 'جدیدترین',*/
                    label: 'Views',
                    value: '3',
                    checked: this.sortValueArray[3]
                },

            ],
            buttons: [
                {
                    /* text: 'مرتب کن',*/
                    text: 'Sort',
                    handler: (data) => {
                        console.log('OK clicked: ' + data);
                        this.sortValue = data;
                        this.sortValueArray = [false, false, false, false];
                        this.sortValueArray[this.sortValue] = true;
                        this.products= null;

                        this.chnageFilter(this.sortValue);
                        this.getData(this.pageParams);
                    }
                },
                {
                    /*text: 'لغو',*/
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked ');
                        this.loading= false;
                    }
                }
            ]
        });
        alert.present();
    }

    clearGlobalFilter() {


        var tempCat = this.service.globalFilter['category'];
        var tempOrder = this.service.globalFilter['order'];
        var tempOrderBy = this.service.globalFilter['orderby'];

        this.service.globalFilter = [];
        this.service.globalFilter['category'] = tempCat;
        this.service.globalFilter['order'] = tempOrder;
        this.service.globalFilter['orderby'] = tempOrderBy;

    }
}
