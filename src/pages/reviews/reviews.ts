import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {ProductService} from "../../providers/service/product-service";
import {Functions} from "../../providers/service/functions";
import {Values} from "../../providers/service/values";
import {md5} from "../product/md5";
import {Service} from "../../providers/service/service";
import {AccountLogin} from "../account/login/login";

/**
 * Generated class for the ReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-reviews',
    templateUrl: 'reviews.html',
})
export class ReviewsPage {
    product: any;
    reviews: any;
    id: any;
    newReview: string;
    newRate: string;
    public disableSubmit: boolean = false;
    errors: any;
    len: number = -1;
    submittedReview : any;
    commentedBefore : boolean = false;
    ratedValue : number = 0;

    constructor(public publicService: Service,
                public nav: NavController,
                public service: ProductService,
                params: NavParams,
                public functions: Functions,
                public values: Values,
                private toastCtrl: ToastController,
                public events: Events) {


        this.id = params.get("id");
        this.reviews = params.get("reviews");
        this.getReviews();

        events.subscribe('star-rating:changed', (starRating) => {
            this.newRate = starRating;
        });

        events.subscribe('star-rating:changed', (starRating) => {
            this.ratedValue = starRating;
        });

        if(this.values.isLoggedIn){
            this.service.hasCommented(this.id, this.publicService.loggedInCustomer.customer.id.toString()).then(data =>{
                if(data == "nocms"){
                    this.commentedBefore = false;
                }
                else{
                    this.submittedReview =data;
                    this.commentedBefore = true;
                    this.ratedValue = this.submittedReview[0].rating;

                    console.log(this.commentedBefore);
                    this.submittedReview[0].review =  this.submittedReview[0].review.replace("<p>" , "");
                    this.submittedReview[0].review =  this.submittedReview[0].review.replace("</p>" , "");

                }

            })
        }


    }

    getReviews() {
        console.log(this.id)
        this.service.getReviews(this.id).then((results) => this.handleReview(results));
    }

    handleReview(a) {
        this.reviews = a;
        for (let i = 0; i < this.reviews.product_reviews.length; i++) {
            console.log(this.reviews.product_reviews[i]);
        }

        for (let item in this.reviews.product_reviews) {
            this.reviews.product_reviews[item].avatar = md5(this.reviews.product_reviews[item].reviewer_email);
        }
        this.len = this.reviews.product_reviews.length;
    }

    validateForm() {
        if (this.newReview == undefined || this.newReview == "") {
           /* this.functions.showAlertWarning("خطا", "امکان ثبت نظر خالی وجو ندارد.");*/
            this.functions.showAlertWarning("Error", "Your comment can not be empty.");
            return false
        }
        if (this.newRate == undefined || this.newRate == "") {
           /* this.functions.showAlertWarning("خطا", "برای ثبت نظر خود، باید به این محصول امتیاز دهید.");*/
            this.functions.showAlertWarning("Error", "To submit your comment, rate this product");
            return false
        }
        return true;
    }

    commitReview() {
        if (this.values.isLoggedIn) {
            this.errors = "";
            if (this.validateForm()) {
                this.disableSubmit = true;
                this.service.submitReview(this.id, this.newReview, this.newRate)
                    .then((results) => this.handleEditInfo(results));
            }

        }
        else {
           /* this.functions.showToast("به منظور ثبت نظر، لطفا ابتدا لاگین کنید.")*/
            this.functions.showToast("To submit review, please login first.")
        }

    }


    showLoginToast() {

        if(!this.values.isLoggedIn){
            let toast = this.toastCtrl.create({
               /* message: 'لطفا به حساب کاربری خود وارد شوید.',*/
                message: 'please login',
                duration: 2000,
                position: 'bottom',
                cssClass: 'rtl',
                showCloseButton: true,
                /*closeButtonText: "ورود"*/
                closeButtonText: "Login"
            });

            toast.onDidDismiss((data, role) => {
                if (role== "close") {
                    this.nav.push(AccountLogin);
                }
            });
            toast.present();
        }

    }


    handleEditInfo(results) {
        console.log(results.errors);
        this.disableSubmit = false;
        if (!results.errors) {

            /*this.functions.showToast('نظر شما ثبت شد و پس از تایید توسط مدیر سیستم نمایش داده خواهد شد. با تشکر از همکاری شما.');*/
            this.functions.showToast("Thank you for your review! Your review is awaiting approval");
            this.nav.pop();

        }
        else if (results.errors) {
            this.errors = results.errors;
        }
    }

    updateReview(id , updatedReview, updatedRating){
        this.service.updateReview(id , updatedReview, updatedRating).then(data=>{
           /* this.functions.showToast("نظر شما ویرایش شد و پس از تایید توسط مدیر سیستم نمایش داده خواهد شد.");*/
            this.functions.showToast("Your review has been editted successfully. Your review is awaiting approval");
            this.getReviews();            // this.nav.pop();
        })
    }
}
