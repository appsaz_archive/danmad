import { Component } from '@angular/core';
import {IonicPage, NavController, ViewController} from 'ionic-angular';
import {AppsazSettings} from "../../appsazSettings";

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  splashIn : any;
  showLogo : boolean =true;

  constructor(public viewCtrl: ViewController,
              public navCtrl:NavController,
              public appsazSettings : AppsazSettings
  ) {

    if(this.appsazSettings.gifsplash){

        setTimeout(() => {
            this.viewCtrl.dismiss();

        }, this.appsazSettings.gifsplashDuration);
    }


    if(this.appsazSettings.imgsplash){
        setTimeout(()=>{
            this.showLogo=true;
            this.splashIn = "animated zoomInDown";

        },1000);

        setTimeout(() => {
            this.viewCtrl.dismiss();

        }, 3000);
    }

  }


}
