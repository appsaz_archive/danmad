import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopsellersPage } from './topsellers';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(TopsellersPage),
  ],
})
export class TopsellersPageModule {}
