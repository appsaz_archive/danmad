import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the AttributeKeyPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'attributeKey',
})
export class AttributeKeyPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string): string {

    return value.replace("attribute_pa_","");
  }
}
