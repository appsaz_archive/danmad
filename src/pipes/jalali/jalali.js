"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var moment = require('jalali-moment');
/**
 * Generated class for the JalaliPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var JalaliPipe = (function () {
    function JalaliPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    JalaliPipe.prototype.transform = function (value, args) {
        var MomentDate = moment(value, 'YYYY/MM/DD');
        return MomentDate.locale('fa').format('YYYY/M/D');
    };
    JalaliPipe = __decorate([
        core_1.Pipe({
            name: 'jalali'
        })
    ], JalaliPipe);
    return JalaliPipe;
}());
exports.JalaliPipe = JalaliPipe;
//# sourceMappingURL=jalali.js.map