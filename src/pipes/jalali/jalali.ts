import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';

/**
 * Generated class for the JalaliPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'jalali',
})
export class JalaliPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */

  transform(value: any, args?: any): any {
    //let MomentDate = moment(value, 'YYYY/MM/DD');
    //return MomentDate.locale('fa').format('YYYY/M/D');
    return moment(value, 'YYYY/MM/DD').format('YYYY/M/D');
  }


  // transform(value: string, ...args) {
  //   return value.toLowerCase();
  // }
}
