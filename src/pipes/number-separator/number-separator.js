"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
/**
 * Generated class for the NumberSeparatorPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var NumberSeparatorPipe = (function () {
    function NumberSeparatorPipe() {
        this.DECIMAL_SEPARATOR = ".";
        this.GROUP_SEPARATOR = ",";
    }
    NumberSeparatorPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (!value) {
            return '';
        }
        if (value == "0.00") {
            return 0;
        }
        var rounded = Math.floor(value);
        var val = rounded.toString();
        var parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
        return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
    };
    NumberSeparatorPipe.prototype.unFormat = function (val) {
        if (!val) {
            return '';
        }
        val = val.replace(/^0+/, '');
        if (this.GROUP_SEPARATOR === ',') {
            return val.replace(/,/g, '');
        }
        else {
            return val.replace(/\./g, '');
        }
    };
    ;
    NumberSeparatorPipe = __decorate([
        core_1.Pipe({
            name: 'numberSeparator'
        })
    ], NumberSeparatorPipe);
    return NumberSeparatorPipe;
}());
exports.NumberSeparatorPipe = NumberSeparatorPipe;
//# sourceMappingURL=number-separator.js.map