import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the NumberSeparatorPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'numberSeparator',
})
export class NumberSeparatorPipe implements PipeTransform {


  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  transform(value: any, ...args) {

    var myObj = {
      style: "currency",
      currency: "DKK",
      currencyDisplay: "code"
     /* useGrouping: false,*/
     /* localeMatcher: "lookup"*/
    }

    return (1 * value).toLocaleString("da-DK", myObj);
    //return value; //for euro


  /*  if (!value) {
      return '';
    }
    if (value == "0.00" || value == 0 || value == "0"){
      return  0;
    }
    let rounded = Math.floor( value );
    let val = rounded.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
*/
  }



 /* unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/^0+/, '');

    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };*/



}
