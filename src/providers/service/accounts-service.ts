import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {URLSearchParams} from '@angular/http';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Http} from '@angular/http';
import {Functions} from "./functions";
import {BehaviorSubject} from "rxjs";
import {Plugins} from "@capacitor/core";
import {Service} from "./service";
const { Storage } = Plugins;

@Injectable()
export class AccountsService {

    cart: any;
    isloggedIn: any;
    status: any;
    user: any;
    login_nonce: any;
    loginLoading : boolean = true;
    public isDeep = new BehaviorSubject(-1);
    filter_types: any;
    private filter: any = {};

    constructor(public http: Http,
                public service : Service,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient) {

        this.filter.page = 1;

    }

    generateActivationCode(phoneNumber: any): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/wc/v3/email/gencode?', {params: new HttpParams().set("username", phoneNumber)})
                .subscribe(data => {
                        console.log("generateActivationCode success");

                        resolve(data);
                    },
                    error => {
                        console.log("generateActivationCode error");
                        console.log(error);
                        reject(false);
                    });
        });

    }

    checkActivationCode(activationCode: any, phoneNumber: any): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/wc/v3/email/chkcode?', {params: new HttpParams().set("username", phoneNumber).set("usercode", activationCode)})
                .subscribe(data => {
                        console.log("checkActivationCode success");

                        resolve(data);
                    },
                    error => {
                        console.log("checkActivationCode error");
                        console.log(error);
                        reject(false);
                    });
        },);

    }

    generateForgetCode(phoneNumber: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.url + '/wp-json/wc/v3/email/genforgetcode', {params: new HttpParams().set("username", phoneNumber)})
                .subscribe(data => {
                        console.log("generateForgetCode success");
                        resolve(data);
                    },
                    error => {
                        console.log("generateForgetCode error");
                        console.log(error);
                        reject(false);
                    });
        });

    }

    checkForgetCode(activationCode: any, phoneNumber: any, newPass: any): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/wc/v3/email/chkforgetcode', {params: new HttpParams().set("username", phoneNumber).set("usercode", activationCode).set("userpass", newPass)})
                .subscribe(data => {
                        console.log("checkForgetCode success");

                        resolve(data);
                    },
                    error => {
                        console.log("checkForgetCode error");
                        console.log(error);
                        reject(false);
                    });
        },);

    }

    registerCustomer(customer) {

        customer['username'] = customer['billing_address']['phone'];
        var params = {
            customer: customer,
        };
        return new Promise(resolve => {
            this.http.post(this.config.setUrl('POST', '/wc-api/v3/customers?', false), params, this.config.options).map(res => res.json()).subscribe(data => {
                this.user = data;
                resolve(this.user);
            }, err => {
                resolve(err.json())
            });
        });
    }

    updateCustomer(editData: any): Promise<any> {
        var id =  this.service.loggedInCustomer.customer.id.toString();
        var params = {
            "customer": {
                billing_address: {
                    address_1: editData["customer"].billing_address.address_1,
                    address_2: editData["customer"].billing_address.address_2,
                    city: editData["customer"].billing_address.city,
                    company: editData["customer"].billing_address.company,
                    country: editData["customer"].billing_address.country,
                    email: editData["customer"].billing_address.email,
                    first_name: editData["customer"].billing_address.first_name,
                    last_name: editData["customer"].billing_address.last_name,
                    phone: editData["customer"].billing_address.phone,
                    postcode: editData["customer"].billing_address.postcode,
                    state: editData["customer"].billing_address.state,
                },
                email: editData["customer"].billing_address.email,
                first_name: editData["customer"].billing_address.first_name,
                last_name: editData["customer"].billing_address.last_name,
                shipping_address: {
                    address_1: editData["customer"].billing_address.address_1,
                    address_2: editData["customer"].billing_address.address_2,
                    city: editData["customer"].billing_address.city,
                    company: editData["customer"].billing_address.company,
                    country: editData["customer"].billing_address.country,
                    first_name: editData["customer"].billing_address.first_name,
                    last_name: editData["customer"].billing_address.last_name,
                    postcode: editData["customer"].billing_address.postcode,
                    state: editData["customer"].billing_address.state,
                }
            }
        };
        return new Promise((resolve, reject) => {
            this.HttpClient.put(this.config.setUrl('PUT', '/wc-api/v3/customers/' + id + '?', this.filter), params).subscribe(data => {
                    console.log("update_customer_success");
                    this.getCustomerById(data['customer'].id);
                    resolve(data);
                },
                err => {
                    console.log("update_customer_error");
                    console.log(err);
                    reject(err);
                });
        });
    }


    login(loginData) {

        console.log("login called");

        if(typeof loginData == "string"){
            var loginDataObj = JSON.parse(loginData);
        }
        else if(typeof loginData == "object"){
            var loginDataObj = loginData;
        }

        var params = new URLSearchParams();
        params.append("username", loginDataObj.username);
        params.append("password", loginDataObj.password);
        params.append("_wpnonce", this.login_nonce);
        params.append("login", 'Login');
        if(loginData.googlePlusLogin && loginData.googlePlusLogin == true){
            params.append("googlePlusLogin", "true");
        }
        else{
            //params.append("googlePlusLogin", "false");
        }
        params.append("redirect", this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-userdata');
        return new Promise(resolve => {
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-login', params, this.config.options)
                .map(res =>
                res.json()).subscribe(data => {

                if (!data.errors) {

                    var status = data.data.status;

                    // this.values.customerName = data.data.display_name;
                    // this.values.customerId = data.data.ID;
                    // this.values.customerEmail = data.data.user_email;

                    this.getCustomerById(data.data.ID).then(data => {
                        this.service.loggedInCustomer = data;
                        this.values.isLoggedIn = status;
                        this.loginLoading = false;

                    }, err =>{
                        this.loginLoading = false;
                    });

                    this.values.logoutUrl = this.encodeUrl(data.data.url);

                    params = new URLSearchParams();
                    params.append("customer_id", data.data.ID.toString());
                    this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-get_wishlist',
                        params, this.config.options).map(res => res.json()).subscribe(data => {
                        for (let item in data) {
                            this.values.wishlistId[data[item].id] = data[item].id;
                        }
                    });

                    //cap*
                    Storage.set({
                        key: 'loginData',
                        value: JSON.stringify({
                            username: loginDataObj.username,
                            password: loginDataObj.password
                        })
                    }).then(data =>{
                        console.log('capacitor Logindata Stored');

                    }, error =>{

                        console.log('capacitor Logindata error');
                        console.log(JSON.stringify(error));
                    });
                }
                else{
                    this.loginLoading = false;
                }


                resolve(data);
            }, err => {
                this.loginLoading = false;
                resolve(JSON.parse(err._body))
            });


        });

    }

    passwordreset(email, nonce, url) {
        var params = new URLSearchParams();
        params.append("user_login", email);
        params.append("wc_reset_password", "true");
        params.append("_wpnonce", nonce);
        params.append("_wp_http_referer", '/my-account/lost-password/');
        return new Promise(resolve => {
            this.http.post(this.config.url + '/my-account/lost-password/', params).subscribe(status => {
                resolve(status);
            });
        });
    }

    passwordResetNonce() {
        return new Promise(resolve => {
            this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-passwordreset', this.config.options)
                .map(res => res.json()).subscribe(data => {
                resolve(data);
            });
        });
    }


    getCustomerByEmail(email) {

        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/wc/v3/customers?email='+email+'&' , false),
                this.config.options).map(res => res.json()).subscribe
            (data => {

            },error =>{
                console.log(error);
            });
        });

    }

    getCustomerById(id) {

        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wc-api/v3/customers/' + id + '?', false), this.config.options)
                .map(res => res.json()).subscribe
            (data => {
                console.log("customer data: ");
                this.service.loggedInCustomer = data;
                console.log(this.service.loggedInCustomer);
                resolve(this.service.loggedInCustomer);
            });
        });

    }


    logout() {
        this.loginLoading = true;
        return new Promise(resolve => {
            this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-logout', this.config.options)
                .subscribe(data => {
                this.values.isLoggedIn = false;
                this.loginLoading = false;

                Storage.set({
                    key: 'loginData',
                    value: JSON.stringify({
                    })
                }).then(data =>{
                }, error =>{
                    console.log(error);
                });


                this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-cart', this.config.options)
                    .map(res => res.json()).subscribe(data => {
                    this.cart = data;
                    this.values.updateCart(this.cart);
                });
                resolve(this.cart);
            }, error=>{
                this.loginLoading = false;
            });
        });
    }


    encodeUrl(href) {
        return href.replace(/&amp;/g, '&')
    }



    subscribeMailpoet(email: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.url + '/wp-json/as_mailpoet/subscribe', {params: new HttpParams().set("email", email)})
                .subscribe(data => {
                        console.log("subscribeMailpoet success");
                        resolve(data);
                    },
                    error => {
                        console.log("subscribeMailpoet error");
                        console.log(error);
                        reject(false);
                    });
        });
    }



    emptyCart() {
        return new Promise((resolve, reject) => {
            this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-emptyCart', this.config.options)
                .subscribe(data => {
                    console.log("emptyCart: " , data)
                    resolve(data);
                }, error=>{
                    reject(false);
                });
        });
    }

}
