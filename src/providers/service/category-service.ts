import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from './config';
import { Values } from './values';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {Service} from "./service";

@Injectable()
export class CategoryService {
    data: any;
    products: any;
    banners: any;
    filters: any;
    status: any;
    product: any;
    cart: any;
    code: any;
    options: any;
    loader: any;
    globalFilter : any =[];
    categories: any;
    mainCategories: any;
    categories_loading : boolean = true;


    constructor(private http: Http,
                private config: Config,
                private HttpClient: HttpClient,
                private values: Values,
                public service : Service) {

        this.mainCategories = [];

    }

    getProducts(params): Promise<any> {
        params.status = 'publish';
        return new Promise((resolve, reject) => {
            //this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/products?', params), this.config.options).subscribe(data => {
             this.HttpClient.get(this.config.setUrl('GET', '/wp-json/as_wc/v3/products?', params), this.config.options).subscribe(data => {
                this.products = data;
                resolve(data);
            }, error => {
                console.log(error);
                reject(false);
            });
        });
    }


    getCategories(page) {
        this.categories_loading = true;
        console.log("getCategories called")
        this.http.get(this.config.setUrl('GET', '/wp-json/wc/v2/products/categories?', {
            per_page: 100, page: page
        })).map(res => res.json()).subscribe(data => {

            if (page == 1){
                this.categories = data;
            }

            else {
                this.categories.push.apply(this.categories, data);
            }

            this.categories = this.categories.filter(item => item.slug !== 'uncategorized');
            this.categories = this.categories.filter(item => item.name !== 'بدون دسته بندی');
            this.mainCategories = this.categories.filter(item => item.parent == 0);

            if (data.length == 100)
                this.getCategories(page + 1);

            this.categories_loading = false;
            console.log("getCategories success")
        });
    }

    load(params)  {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/as_wc/v3/products?', params), this.config.options).map(res => res.json())
                .subscribe(data => {
                        this.products = data;

                        resolve(this.products);
                    },
                    error=>{
                        console.log(error);
                    });
        });
    }
    loadMore(filter) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/as_wc/v3/products?', filter), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.products = data;
                    resolve(this.products);
                });
        });
    }

    addToCart(params) {
        return new Promise(resolve => {
            var searchParams = new URLSearchParams();
            for (let param in params) {
                searchParams.set(param, params[param]);
            }
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart', searchParams, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data.cart;
                    this.values.cartNonce = data.cart_nonce;
                    this.values.updateCartTwo(this.status);
                    resolve(this.status);
                });
        });
    }
    deleteFromCart(id) {
        var params = new URLSearchParams();
        for (let key in this.values.cartItem) {
            if (this.values.cartItem[key].product_id == id) {
                this.values.count -= 1;
                if (this.values.cartItem[key].quantity != undefined && this.values.cartItem[key].quantity == 0) {
                    this.values.cartItem[key].quantity = 0
                }
                else {
                    this.values.cartItem[key].quantity -= 1
                };
                if (this.values.cart[id] != undefined && this.values.cart[id] == 0) {
                    this.values.cart[id] = 0
                }
                else {
                    this.values.cart[id] -= 1
                };
                params.set('cart[' + key + '][qty]', this.values.cartItem[key].quantity);
            }
        }
        params.set('_wpnonce', this.values.cartNonce);
        params.set('update_cart', 'Update Cart');
        params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-cart');
        return new Promise(resolve => {
            this.http.post(this.config.url + '/cart/', params, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data;
                    resolve(this.status);
                });
        });
    }
    updateToCart(id) {
        var params = new URLSearchParams();
        for (let key in this.values.cartItem) {
            if (this.values.cartItem[key].product_id == id) {
                this.values.count += 1;
                if (this.values.cartItem[key].quantity != undefined && this.values.cartItem[key].quantity == 0) {
                    this.values.cartItem[key].quantity = 0
                }
                else {
                    this.values.cartItem[key].quantity += 1
                };
                if (this.values.cart[id] != undefined && this.values.cart[id] == 0) {
                    this.values.cart[id] = 0
                }
                else {
                    this.values.cart[id] += 1
                };
                params.set('cart[' + key + '][qty]', this.values.cartItem[key].quantity);
            }
        }
        console.log(this.values.cartNonce);
        params.set('_wpnonce', this.values.cartNonce);
        params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-cart');
        params.set('update_cart', 'Update Cart');
        return new Promise(resolve => {
            this.http.post(this.config.url + '/cart/', params, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data;
                    resolve(this.status);
                });
        });
    }


    search(params) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET',  '/wp-json/as_wc/v3/products?', params), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.products = data;
                    resolve(this.products);
                });
        });
    }


    addToWishlist(id) {
        return new Promise(resolve => {
          var params = new URLSearchParams();
          params.append("product_id", id);
          params.append("customer_id", this.service.loggedInCustomer.customer.id.toString());
          this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_wishlist', params, this.config.options).map(res => res.json())
            .subscribe(data => {
              this.status = data;
              resolve(this.status);
            });
        });
    }


    deleteItem(id){
        var params = new URLSearchParams();
        params.append("product_id", id);
        params.append("customer_id", this.service.loggedInCustomer.customer.id.toString());
          return new Promise(resolve => {
          this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-remove_wishlist', params, this.config.options).map(res => res.json())
            .subscribe(data => {          
              resolve(data);
            });
        });
    }
}
