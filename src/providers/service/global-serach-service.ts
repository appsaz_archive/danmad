import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from './config';
import 'rxjs/add/operator/map';

@Injectable()
export class GlobalSearchService {

    products: any;
    globalFilter : any =[];


    constructor(private http: Http,
                private config: Config,
                ) {

    }


    loadMore(filter) {
         filter.status = 'publish';
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/wc/v3/products?', filter), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.products = data;
                    resolve(this.products);
                });
        });
    }



    search(params) {
         params.status = 'publish';
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET',  '/wp-json/wc/v3/products?', params), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.products = data;
                    resolve(this.products);
                });
        });
    }

}
