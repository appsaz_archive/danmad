import {Injectable} from '@angular/core';
// import {Http} from '@angular/http';
import {LoadingController, Platform, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {URLSearchParams} from '@angular/http';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Http} from '@angular/http';
import {Functions} from "./functions";
import { Device } from '@ionic-native/device';
import { ajax } from "rxjs/observable/dom/ajax";

@Injectable()
export class notificationService {

    uuid : any ;
    newMessages : any = [];

    constructor(public http: Http,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient,
                private device: Device,
                private platform : Platform) {

        if (this.platform.is("cordova")){
            this.uuid=this.device.uuid;
        }
        else{
            this.uuid='web52385465';
        }
    }

    deviceRegistry (deviceId): Promise<any> {0
        var deviceType = this.platform.platforms()[2];
        var userRegId = this.uuid;

        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/notif/devicereg', {params: new HttpParams()
                    .set("deviceId", deviceId).set("deviceType", deviceType).set("id",userRegId)})
                .subscribe(data => {
                        console.log("device reg success");
                        resolve(data);
                    },
                    error => {
                        console.log("device reg error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    setSeen (messageId): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/notif/notifseen', {params: new HttpParams()
                    .set("userId", this.uuid ).set("messageId", messageId)})
                .subscribe(data => {
                        console.log("setSeen success");
                        resolve(data);
                    },
                    error => {
                        console.log("setSeen error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    getMessages (offset , limit): Promise<any> {


        console.log(offset + "<=offset || limit=>" +limit);
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/notif/notifall', {params: new HttpParams()
                    .set("userId", this.uuid ).set("offset",offset).set("limit",limit)})
                .subscribe(data => {
                        console.log("notifall success");
                        resolve(data);
                    },
                    error => {
                        console.log("notifall error");
                        console.log(error);
                        reject(false);
                    });
        });

    }



    setDelivered (messageId): Promise<any> {

        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/notif/notifdelivery', {params: new HttpParams()
                    .set("userId", this.uuid ).set("messageId", messageId)})
                .subscribe(data => {
                        console.log("notifdelivery success");
                        resolve(data);
                    },
                    error => {
                        console.log("notifdelivery error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    getNewMessages (offset , limit): Promise<any> {


        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/notif/get_unseen', {params: new HttpParams()
                    .set("userId", this.uuid ).set("offset",offset).set("limit",limit)})
                .subscribe(data => {
                        console.log("get_unseen success");
                        this.newMessages = data;
                        resolve(data);
                    },
                    error => {
                        console.log("get_unseen error");
                        console.log(error);
                        reject(error);
                    });
        });

    }

}
