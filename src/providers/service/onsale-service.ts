import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {URLSearchParams} from '@angular/http';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Http} from '@angular/http';
import {Functions} from "./functions";
import {BehaviorSubject} from "rxjs";
import {Plugins} from "@capacitor/core";
const { Storage } = Plugins;

@Injectable()
export class OnsaleService {
    onSales: any;
    onsale_filter: any = {};
    has_more_onsale_items: boolean = true;
    onsale_loading : boolean = true;

    constructor(public http: Http,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient) {

        this.onsale_filter.page = 1;

    }
    getOnsale(): Promise<any> {
        this.onsale_loading = true;
        return new Promise((resolve, reject) => {

            this.onsale_filter.on_sale = true;
            this.onsale_filter.status = 'publish';

            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/products?', this.onsale_filter)).subscribe(data => {
                this.onSales = data;
                this.onsale_loading = false;
                console.log("onSale success");
                resolve(true);

            }, error => {
                console.log(error);
                reject(false);
            });
        });
    }


    loadMore_onsale() {
        this.onsale_filter.page += 1;
        this.onsale_filter.on_sale = true;
        this.onsale_filter.status = 'publish';

        return new Promise(resolve => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v2/products/?', this.onsale_filter)).subscribe(data => {

                this.handleMore_onsale(data);
                resolve(true);
            });
        });
    }

    handleMore_onsale(results) {
        if (results != undefined) {

            for (var i = 0; i < results.length; i++) {
                this.onSales.push(results[i]);
            }
        }
        if (results.length == 0) {
            this.has_more_onsale_items = false;
        }
    }

}
