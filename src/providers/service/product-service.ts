import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Config} from './config';
import {Values} from './values';
import {URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import {LoadingController, ToastController} from 'ionic-angular';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Functions} from "./functions";
import {Service} from "./service";

import { Headers } from '@angular/http';



@Injectable()
export class ProductService {
    data: any;
    product: any;
    gallery: any;
    status: any;
    wishlist: any;
    reviews: any;
    reviewForm: any;
    cart: any;
    code: any;
    loader: any;
    filter: any = {};
    related: any;
    options: any = {};

    constructor(private http: Http,
                private config: Config,
                public publicService : Service,
                private functions: Functions,
                private values: Values,
                private loadingController: LoadingController,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient) {

    }

    getProduct(id) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wc-api/v3/products/' + id + '?', false), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.product = data;
                    resolve(this.product);
                });
        });
    }

    getReviews(id) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wc-api/v3/products/' + id + '/reviews' + '?', false), this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.reviews = data;
                    console.log("getReviews data");
                    console.log(data);
                    resolve(this.reviews);
                });
        });
    }

    addToCart(params) {
        return new Promise(resolve => {
            var searchParams = new URLSearchParams();
            for (let param in params) {
                searchParams.set(param, params[param]);
            }
            console.log(searchParams)
            console.log(params)
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart', searchParams, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data;
                    console.log(data)
                    resolve(this.status);
                });

        });

    }


    addToWishlist(id) {
        return new Promise(resolve => {
            var params = new URLSearchParams();
            params.append("product_id", id);
            params.append("customer_id", this.publicService.loggedInCustomer.customer.id.toString());
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_wishlist', params, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data;
                    resolve(this.status);
                });
        });
    }

    deleteItem(id) {
        var params = new URLSearchParams();
        params.append("product_id", id);
        params.append("customer_id", this.publicService.loggedInCustomer.customer.id.toString());
        return new Promise(resolve => {
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-remove_wishlist', params, this.config.options).map(res => res.json())
                .subscribe(data => {

                    resolve(data);
                });
        });
    }


    submitReview(product_id, review, rating): Promise<any> {

        var params = new URLSearchParams();
        params.append("product_id", product_id);
        params.append("status", "hold");
        params.append("user_id",  this.publicService.loggedInCustomer.customer.id.toString());
        params.append("review", review);
        params.append("reviewer", this.publicService.loggedInCustomer['customer']['first_name'] + " " +this.publicService.loggedInCustomer['customer']['last_name']) ;
        params.append("rating", rating);

        var headers = new Headers();
        headers.append("content", 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return new Promise((resolve, reject) => {

            this.http.post(this.config.url + '/wp-json/woocomment/submitComment?', params).map(res => res.json()).subscribe(data => {

                console.log("submitReview data");
                console.log(data);
                resolve(data);

            }, err => {

                console.log("submitReview err");
                console.log(err);
                reject(err.json())
            });

        });
    }




    hasCommented(id, reviewer_id) {

        var params = new URLSearchParams();
        params.append("product", id);
        params.append("reviewer", reviewer_id);

        var headers = new Headers();
        headers.append("content", 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return new Promise((resolve, reject) => {

            this.http.post(this.config.url + '/wp-json/woocomment/getUserComment?', params).map(res => res.json()).subscribe(data => {

                console.log("hasCommented data");
                console.log(data);
                resolve(data);

            }, err => {

                console.log("hasCommented err");
                console.log(err);
                reject(err.json())
            });

        });

    }

    updateReview(id, updatedReview, updatedRating) {
        console.log("updateReview");

        var params = {
            "review": updatedReview,
            "rating": updatedRating,
            "status": "hold",
        };

        return new Promise((resolve, reject) => {
            this.HttpClient.post(this.config.setUrl('POST', '/wp-json/wc/v3/products/reviews/' + id + '?', this.filter), params).subscribe(data => {
                    console.log("update_review_success");
                    console.log(data);
                    resolve(data);
                },
                err => {
                    console.log("update_review_error");
                    console.log(err);
                    reject(err);
                });
        });
    }



    getRelatedProducts(params) {

        console.log("getRelatedProducts");

        // if ( this.publicService.loggedInCustomer.customer.id.toString()) {
        //     params.user_id =  this.publicService.loggedInCustomer.customer.id.toString();
        // }

        return new Promise(resolve => {
            this.http
                .get(
                    this.config.setUrl(
                        'GET',
                        '/wp-json/wc/v3/products?',
                        params
                    ),
                    this.config.options
                )
                .map(res => res.json())
                .subscribe(data => {
                    this.related = data;
                    console.log("getRelatedProducts success")
                    console.log(data)
                    resolve(this.related);
                });
        });
    }



    /*   getPostViews(postId) {
     return new Promise(resolve => {
     this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=ag_get_post_views&postId='+postId, this.config.options).subscribe(data => {
     resolve(data);
     });
     });
     }*/

    getPostViews(postId) {
        return new Promise(resolve => {
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            this.options.headers = headers;
            var params = new URLSearchParams();
            params.append("postId", postId);
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=ag_get_post_views', params, this.options).map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                });
        });
    }


    addPostViews(postId) {
        return new Promise(resolve => {
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            this.options.headers = headers;
            var params = new URLSearchParams();
            params.append("postId", postId);
            params.append("uId", this.publicService.loggedInCustomer.customer.id.toString() );
            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=ag_add_post_views', params, this.options).map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                });
        });
    }


    }
