import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Functions} from "./functions";
import {BehaviorSubject} from "rxjs";
import {URLSearchParams} from '@angular/http';
import {Http, RequestOptions} from '@angular/http';
import {Headers} from '@angular/http';

@Injectable()
export class Service {
    data: any;
    orders: any;
    order: any;
    // isloggedIn: any;
    status: any;
    products: any;
    cart: any;
    country: any;
    user: any;
    login_nonce: any;
    private filter: any = {};
    has_more_items: boolean = true;
    blocks: any = [];
    homeSliders: any = [];
    loggedInCustomer: any;
    globalFilterString: any = [];
    supportNumber: any = '000';
    all_products_loading: boolean = true;
    banners_loading: boolean = true;
    public isDeep = new BehaviorSubject(-1);
    filter_types: any;

    constructor(public http: Http,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient) {


        this.filter.page = 1;

    }

    getBanners(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-keys').subscribe(
                data => {
                    this.homeSliders = data['blocks'][0]['children'];
                    this.banners_loading = false;
                    resolve(this.homeSliders);
                },
                error => {
                    console.log(error);
                    reject(false);
                })
        });
    }

    load() {
        return new Promise(resolve => {
            this.HttpClient.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-keys').subscribe(data => {
                console.log("AAAA", data)
                this.values.data = data;
                this.values.settings = data['settings'];
                this.supportNumber = data['settings']['whatsapp_number'];
                this.values.settings = data['settings'];
                this.blocks = data['blocks'];
                // this.values.currency = data['currency'];
                this.login_nonce = data['login_nonce'];
                if (data['user'] && data['user'].data != undefined) {
                    this.values.isLoggedIn = data['user'].data.status;
                    this.loggedInCustomer = data['user'].data;
                    //this.values.avatar = data.user.data.avatar_url;
                    this.values.logoutUrl = this.encodeUrl(data['user'].data.url);
                }
                this.values.data.max_price = data['max_price'];
                this.values.data.min_price = data['min_price']

                resolve(true);
            });
        });
    }


    getNonce() {
        return new Promise(resolve => {
            this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-nonce', this.config.options).map(res => res.json()).subscribe(data => {
                resolve(data);
            });
        });
    }


    encodeUrl(href) {
        return href.replace(/&amp;/g, '&')
    }


    getOrder(id) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wc-api/v3/orders/' + id + '?', false), this.config.options).map(res => res.json()).subscribe(data => {
                this.order = data;
                resolve(this.order);
            });
        });
    }


    getCountry() {
        return new Promise(resolve => {
            this.http.get(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-get_country', this.config.options).map(res => res.json()).subscribe(data => {
                this.country = data;
                resolve(this.country);
            });
        });
    }


    getOrders(filter) {

        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/wc/v2/orders?', filter), this.config.options).map(res => res.json()).subscribe(
                data => {
                    this.orders = data;
                    resolve(this.orders);

                }, error => {
                    console.log(error);
                });
        });
    }

    updateOrder(data, id) {

        return new Promise(resolve => {
            this.HttpClient.put(this.config.setUrl('PUT', '/wc-api/v3/orders/' + id + '?', false), data, {}).subscribe(data => {
                this.status = data;
                resolve(this.status);
            }, err => {
                console.log((err.error));
                resolve((err.error))
            });
        });
    }

    getProducts(): Promise<any> {
        this.all_products_loading = true;
        return new Promise((resolve, reject) => {
            var filter;
            filter.status = 'publish';
            this.HttpClient.get(this.config.setUrl('GET', '/wc-api/v3/products?', filter), this.config.options).subscribe(data => {
                this.products = data;
                this.all_products_loading = false;
                console.log("getProducts success")
                resolve(data);
            }, error => {
                console.log(error);
                reject(false);
            });
        });
    }


    loadMore() {
        this.filter.page += 1;
        this.filter.status = 'publish';
        return new Promise(resolve => {
            this.HttpClient.get(this.config.setUrl('GET', '/wc-api/v3/products?', this.filter), this.config.options).subscribe(data => {
                this.handleMore(data);
                resolve(true);
            });
        });
    }


    handleMore(results) {
        console.log("handleMore")
        console.log(results)
        if (results != undefined) {
            for (var i = 0; i < results.products.length; i++) {
                this.products.products.push(results.products[i]);
            }
            ;
        }
        if (results.length == 0) {
            this.has_more_items = false;
        }
    }


    getProductById(id) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wc-api/v3/products/' + id + '?', false), this.config.options).map(res => res.json()).subscribe
            (data => {
                resolve(data);
            });
        });
    }


    getAttributes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/products/attributes?', this.filter), {}).subscribe(data => {

                    resolve(data);
                },
                err => {
                    console.log(err);
                    reject('');
                });
        });
    }

    getAttributeVar(attr_id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/products/attributes/' + attr_id + '/terms?', this.filter), {}).subscribe(data => {

                    resolve(data);
                },
                err => {
                    console.log(err);
                    reject('');
                });
        });

    }

    getProductsByAttribute(filters: string, category, order, orderby): Promise<any> {
        var params = [];
        params['filter[category]'] = category;
        params['filter[order]'] = order;
        params['filter[order_by]'] = orderby;
        params['filter[status]'] = 'publish';


        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.setUrl('GET', '/wc-api/v3/products?' + filters + '&', params), this.config.options).subscribe(data => {
                console.log(data);
                resolve(data);
            }, error => {
                console.log(error);
                reject(false);
            });
        });
    }

    getAboutText(): Promise<any> {

        console.log("getAboutText")
        var params;
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/ag_palapal/get/aboutus')
                .subscribe(data => {
                        console.log("getAboutText success");

                        resolve(data);
                    },
                    error => {
                        console.log("getAboutText error");
                        console.log(error);
                        reject(false);
                    });
        });

    }

    setIsDeep(param: any) {
        this.isDeep.next(param);
    }


    getBlogs(page): Promise<any> {

        console.log("getBlogs")
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/wp/v2/posts?page=' + page)
                .subscribe(data => {
                        console.log("getBlogs success");

                        resolve(data);
                    },
                    error => {
                        console.log("getBlogs error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    checkForUpdates(): Promise<any> {

        console.log("checkForMandatoryUpdates")
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/appsaz/mandatoryupdate')
                .subscribe(data => {
                        console.log("checkForMandatoryUpdates success");
                        resolve(data);
                    },
                    error => {
                        console.log("checkForMandatoryUpdates error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    getLinkablebanners(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.HttpClient.get(this.config.url + '/wp-json/banners/v1/all').subscribe(
                data => {
                    //this.homeSliders = data['blocks'][0]['children'];
                    this.homeSliders = data['slider'];
                    console.log("homeSliders: ", this.homeSliders)
                    this.banners_loading = false;
                    resolve(this.homeSliders);
                },
                error => {
                    console.log(error);
                    reject(false);
                })
        });
    }


    checkOrderItemsToReorder(order_id) {
        console.log("checkOrderItemsToReorder service called", order_id);
        var params = new URLSearchParams()
        params.append("order_id", order_id);
        return new Promise(resolve => {
            this.http.post(this.config.url + '/wp-json/as_appsaz/get-order-items', params, this.config.options)
                .map(res => res.json()).subscribe(data => {
                console.log("checkOrderItemsToReorder data", data);
                resolve(data);
            }, error => {
                console.log("checkOrderItemsToReorder err", error);
            });
        });
    }

    /*   checkOrderItemsToReorder(order_id) {
     var params = new URLSearchParams()
     params.append("order_id", order_id);
     return new Promise(resolve => {
     this.http.post(this.config.setUrl('POST', '/wp-json/as_appsaz/get-order-items?', false), params, this.config.options).map(res => res.json()).subscribe(data => {
     resolve(data);
     }, err => {
     resolve(err.json())
     });
     });
     }*/

    duplicateOrder(order_id) {
        console.log("duplicateOrder service called", order_id);
        var params = new URLSearchParams()
        params.append("order_id", order_id);
        return new Promise(resolve => {
            this.http.post(this.config.url + '/wp-json/as_appsaz/duplicate-order', params, this.config.options)
                .map(res => res.json()).subscribe(data => {
                console.log("duplicateOrder data");
                console.log(data);
                resolve(data);
            }, error => {
                console.log("duplicateOrder err");
                console.log(error)
            });
        });
    }


    addProductsToCart(product_ids) {
        console.log("duplicateOrder service called", typeof product_ids, product_ids, this.loggedInCustomer.customer.id);
        var params = new URLSearchParams()
        /*params.append("product_ids", product_ids);*/
        //params.append("productsList", JSON.stringify(product_ids));
        params.append("productsList", product_ids);
        params.append("user_id", this.loggedInCustomer.customer.id);
        return new Promise(resolve => {
            this.http.post(this.config.url + '/wp-json/as_appsaz/add-products-to-cart', params, this.config.options)
                .map(res => res.json()).subscribe(data => {
                console.log("duplicateOrder data");
                console.log(data);
                resolve(data);
            }, error => {
                console.log("duplicateOrder err");
                console.log(error)
            });
        });
    }


    getDeliveryTimes(): Promise<any> {

        console.log("getDeliveryTimes")
        return new Promise((resolve, reject) => {

            //this.HttpClient.get(this.config.url + 'wp-json/time/v1/receive')
            this.HttpClient.get(this.config.url + '/wp-json/post/v1/scheduling')
                .subscribe(data => {
                        console.log("getDeliveryTimes success");
                        console.log(data);
                        //resolve(data['time_send']);
                        resolve(data);
                    },
                    error => {
                        console.log("getDeliveryTimes error");
                        console.log(error);
                        reject(false);
                    });
        });

    }


    getSpecialItems(offset, count) {
        return new Promise(resolve => {
            this.http.get(this.config.setUrl('GET', '/wp-json/customer/related-products?user_id=' + this.loggedInCustomer.customer.id + '&page=' + offset + '&number=' + count + '&', false), this.config.options).map(res => res.json()).subscribe(
                data => {
                    var list = [];
                    for (var i = 0; i < data.length; i++) {
                        list.push(data[i])
                    }
                    console.log("listB ", typeof list, list)
                    resolve(list);
                }, error => {
                    console.log(error);
                });
        });
    }


    /*    addToCartBuilk() {
           /!* var params = new URLSearchParams();
            if (this.values.cartItem[key].quantity != undefined && this.values.cartItem[key].quantity == 0) {
                this.values.cartItem[key].quantity = 0
            }
            else {
                this.values.cartItem[key].quantity += 1
            };
            if (this.values.cart[id] != undefined && this.values.cart[id] == 0) {
                this.values.cart[id] = 0
            }
            else {
                this.values.cart[id] += 1;
            };
            params.set('cart[' + key + '][qty]', this.values.cartItem[key].quantity);
            params.set('_wpnonce', this.values.cartNonce);
            params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart_builk');
            params.set('update_cart', 'Update Cart');*!/

            var productsList=[];
            productsList= [{"product_id":4485, "quantity":1}];

           /!* var params = new URLSearchParams();
            //params.set('cart[' + key + '][qty]', this.values.cartItem[key].quantity);
            params.set("products_list" , JSON.stringify(productsList) );
            params.set('_wpnonce', this.values.cartNonce);
            params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart_builk');
            params.set('update_cart', 'Update Cart');


            return new Promise(resolve => {

                this.http.post(this.config.url + '/cart/', params, this.config.options).map(res => res.json())
                    .subscribe(data => {
                        this.cart = data;
                        console.log("addToCart")
                        console.log(data)
                        this.values.updateCart(this.cart);
                        resolve(this.cart);
                    });
            });*!/

            var params = new URLSearchParams();
            params.append("products_list", JSON.stringify(productsList) );

            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart_builk',
                params, this.config.options).map(res => res.json()).subscribe(data => {
                for (let item in data) {
                    this.values.wishlistId[data[item].id] = data[item].id;
                }
            });

        }*/


    /*  addToCartBulk() {
          var productsList=[];
          productsList= [{"product_id":4485, "quantity":1}];
          var params = new URLSearchParams();
          params.append("products_list", JSON.stringify(productsList) );
          this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart_builk',
              params, this.config.options).map(res => res.json()).subscribe(data => {
              for (let item in data) {
                  this.values.wishlistId[data[item].id] = data[item].id;
              }
          });
      }
  */


    addToCartBulk(params) {
        let headers = new Headers(
            {
                'Content-Type': 'application/json'
            });
        let options = new RequestOptions({headers: headers});
        let data = JSON.stringify(params);
        console.log('data', data);
        console.log('params', params);

        return new Promise(resolve => {
            // var searchParams = new URLSearchParams();
            // for (let param in params) {
            //     searchParams.set(param, params[param]);
            // }
            // console.log('searchParams', searchParams);
            let httpParams = new HttpParams();
            let data = httpParams.set('products_list', params);
            console.log('params', params);
            console.log('data', data);

            this.http.post(this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-add_to_cart_bulk',
                params, this.config.options).map(res => res.json())
                .subscribe(data => {
                    this.status = data;
                    console.log('data', data);
                    resolve(this.status);
                });

        });

    }


}
