import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Config} from './config';
import {Values} from './values';
import {NativeStorage} from '@ionic-native/native-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import {HttpClient} from '@angular/common/http';
import {Http} from '@angular/http';
import {Functions} from "./functions";
import {Plugins} from "@capacitor/core";
import {Service} from "./service";
@Injectable()
export class TopsellersService {

    topsellers: any = [];
    topsellers_filter: any = {};
    top_seller_loading : boolean = true;
    private filter: any = {};
    has_more_topseller_items: boolean = true;

    constructor(public http: Http,
                private config: Config,
                private values: Values,
                private functions: Functions,
                public loadingCtrl: LoadingController,
                private nativeStorage: NativeStorage,
                private toastCtrl: ToastController,
                private HttpClient: HttpClient,
                public service : Service) {

        this.topsellers_filter.page = 1;

    }


    loadMore_topsellers() {

        this.topsellers_filter.page += 1;
        this.topsellers_filter.period = 'year';
        this.topsellers_filter.status = 'publish';

        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/reports/top_sellers?', this.topsellers_filter), {}).subscribe(data => {

                    var size = 0 + Object.keys(data).length;
                    for (let i = 0; i < size; i++) {
                        this.service.getProductById(data[i]["product_id"]).then(
                            res => {
                                this.topsellers.push(res);
                            },
                            error => {
                                console.log(error);
                            });
                    }
                    resolve(true);
                },
                err => {
                    console.log(err);
                    reject(false);
                });
        });


    }

    handleMore_topsellers(results) {
        if (results != undefined) {

            for (var i = 0; i < results.length; i++) {
                this.topsellers.push(results[i]);
            }
        }
        if (results.length == 0) {
            this.has_more_topseller_items = false;
        }
    }

    getTopsellers(): Promise<any> {
        this.top_seller_loading = true;
        this.filter.period = 'year';
        return new Promise((resolve, reject) => {
            this.HttpClient.get(this.config.setUrl('GET', '/wp-json/wc/v3/reports/top_sellers?', this.filter), {}).subscribe(data => {

                    var size = 0 + Object.keys(data).length;
                    this.topsellers = [];
                    for (let i = 0; i < size; i++) {
                        this.service.getProductById(data[i]["product_id"]).then(
                            res => {
                                this.topsellers.push(res);
                            },
                            error => {
                                console.log(error);
                            });
                    }
                    this.top_seller_loading = false;
                    resolve(true);
                },
                err => {
                    console.log(err);
                    reject(false);
                });
        });
    }



}
