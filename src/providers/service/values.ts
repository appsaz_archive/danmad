import { Injectable } from '@angular/core';

@Injectable()
export class Values {
  count: number = 0;
  filter: any = 10;
  isLoggedIn: boolean = false;
  cart: Array<number> = [];
  logoutUrl: any;
  cartItem: any;
  cartNonce: any;
  avatar: any = "assets/image/logo.png";
  data: any;
  deviceId: any;
  wishlistId: any = [];
  settings: any = {"show_featured":"1",
      "show_onsale":"1",
      "show_latest":"1",
      "pull_to_refresh":"1",
      "onesignal_app_id":"ceae1a87-5cc5-44fe-9c6b-e9559853e8d9",
      "google_project_id":"575406525034",
      "google_web_client_id":"575406529046-r69ss57ceip6vv9mm858qh2hg3j0sl7u.apps.googleusercontent.com",
      "rate_app_ios_id":"","rate_app_android_id":"https://play.google.com/store/apps/details?id=com.mstore.premiumtheme001&hl=en",
      "rate_app_windows_id":"","share_app_android_link":"https://play.google.com/store/apps/details?id=com.mstore.premiumtheme001&hl=en",
      "share_app_ios_link":"https://play.google.com/store/apps/details?id=com.mstore.premiumtheme001&hl=en",
      "support_email":"support@example.com",
      "image_height":"100",
      "product_slider_width":"42",
      "enable_product_chat":"",
      "enable_home_chat":"",
      "whatsapp_number":"+91XXXXXXXX",
      "app_dir":"ltr",
      "language":"english"};
  constructor() {
  	this.filter = {};
    this.logoutUrl = "";
  }
  updateCart(crt) {
     // this.cartItem = crt.cart_contents;
     // this.cart = [];
     // this.count = 0;
     // for (let item in crt.cart_contents) {
     //     this.cart[crt.cart_contents[item].product_id] = parseInt(crt.cart_contents[item].quantity);
     //     this.count += parseInt(crt.cart_contents[item].quantity);
     // }
 }
 updateCartTwo(crt) {
     this.cart = [];
     // this.count = 0;
     // this.cartItem = crt;
     // for (let item in crt) {
     //     this.cart[crt[item].product_id] = parseInt(crt[item].quantity);
     //     this.count += parseInt(crt[item].quantity);
     // }
 }

 }
